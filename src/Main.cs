/*
 * Copyright (c) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 * Copyright (c) 2006 Patrick Wagstrom <pwagstro@andrew.cmu.edu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.Net;
using System.Collections;

using Gdk;
using Gtk;
using Glade;
using Gecko;

using Mono.Posix;

namespace MCatalog {
public class GladeApp
{
	[Glade.Widget] private Gnome.App mcatalogMainWindow;
	private Gnome.Program program;
	private Database database;
	private CatalogList catalogList;
	private Catalog activeCatalog;
	private BorrowerList borrowerList;

	// Lists widgets
	[Glade.Widget] private Notebook notebook;
	[Glade.Widget] private ScrolledWindow swCatalogs;
	[Glade.Widget] private ScrolledWindow swBorrowers;
	[Glade.Widget] private ScrolledWindow swItemsList;
	[Glade.Widget] private VBox vbPresentation;
	[Glade.Widget] private ScrolledWindow swCairoShelf;
	[Glade.Widget] private HPaned itemListPaned;
	[Glade.Widget] private HPaned catalogListPaned;
	[Glade.Widget] private VPaned borrowersPaned;
	[Glade.Widget] private Frame borrowersFrame;

	private TitleWidget titleWidget;
	//	private TitleWidgetOrderChangedHandler titleWidgetOrderChangedHandler;
	private View activeView;

	private Presentation presentation;
	private ItemList itemList;

	// Buttons
	[Glade.Widget] private MenuItem menuItemAddItem;
	[Glade.Widget] private MenuItem menuItemRemoveItem;
	[Glade.Widget] private RadioMenuItem menuItemShelfView;
	[Glade.Widget] private RadioMenuItem menuItemListView;
	private ToggleButton buttonList;
	private ToggleButton buttonShelf;

	[Glade.Widget] private Button zoomInButton;
	[Glade.Widget] private Button zoomOutButton;
	[Glade.Widget] private Button addItemButton;
	[Glade.Widget] private Button removeItemButton;

	//	[Glade.Widget] private Button addBorrowerButton;
	//	[Glade.Widget] private Button removeBorrowerButton;

	[Glade.Widget] private Button editItemButton;
	[Glade.Widget] private Button lendItemButton;

	[Glade.Widget] private Gtk.Entry searchEntry;
	// searchOn: true if a search is active.
	private bool searchOn;

	protected CairoShelf cairoShelf;

	public static void Main (string[] args)
	{
		new GladeApp (args);
	}

	public GladeApp (string[] args) 
	{
		program = new Gnome.Program ("mCatalog", "1.0", Gnome.Modules.UI, args);
		Mono.Unix.Catalog.Init ("mcatalog", Defines.GNOME_LOCALE_DIR);

		// Proxy Setup
		bool use_proxy = Conf.Get ("/system/http_proxy/use_http_proxy", false);

		if (use_proxy) {
			string proxy_host = Conf.Get ("/system/http_proxy/host", "");
			int proxy_port = Conf.Get ("/system/http_proxy/port", 8080);
			string proxy = string.Format("http://{0}:{1}/", proxy_host, proxy_port);
			WebProxy proxyObject = new WebProxy(proxy, true);
			System.Net.GlobalProxySelection.Select = proxyObject;
		}

		database = new Database (Conf.HomeDir+"/db.db");
		database.Debug = true;

		Glade.XML gxml = new Glade.XML (null, "mainwindow.glade", "mcatalogMainWindow", "mcatalog");
		gxml.Autoconnect (this);
		mcatalogMainWindow.DeleteEvent += OnWindowDeleteEvent;

		presentation = new Presentation ();
        vbPresentation.PackStart(presentation);
		presentation.Init ();

		// Create some needed widgets
		buttonList = new ToggleButton ();
		buttonShelf = new ToggleButton ();
		titleWidget = new TitleWidget ();

		// Populate the catalog tree
		PopulateCatalogs ();
		PopulateBorrowers ();

		// Initialize the widgets
		buttonList.Clicked += OnButtonListClicked;
		buttonShelf.Clicked += OnButtonShelfClicked;
		titleWidget.OnOrderChanged += OnOrderChanged;

		searchEntry.Activated += OnSearchEntryActivated;

		itemListPaned.SizeRequested += OnItemListPanedResized;

		// Fill the list hbox
		HBox hBoxList = (HBox)gxml["hBoxList"];
		Gtk.Image image1 = new Gtk.Image (new Gdk.Pixbuf (null, "list.png"));
		image1.Visible = true;
		buttonList.Add (image1);
		buttonList.Relief = ReliefStyle.Half;
		hBoxList.PackStart (buttonList, false, true, 0);
		
		Gtk.Image image2 = new Gtk.Image (new Gdk.Pixbuf (null, "shelf.png"));
		image2.Visible = true;
		buttonShelf.Add (image2);
		buttonShelf.Relief = ReliefStyle.Half;
		hBoxList.PackStart (buttonShelf, false, true, 0);

		hBoxList.PackStart (titleWidget, true, true, 4);
		hBoxList.ShowAll();

		// Get the menu items we need to handle
		menuItemAddItem.Sensitive = false;
		menuItemRemoveItem.Sensitive = false;
		menuItemShelfView.Data["view"] = View.Shelf;
		menuItemListView.Data["view"] = View.List;

		menuItemShelfView.Toggled += OnViewToggled;

		// Buttons
		addItemButton.Sensitive = false;
		removeItemButton.Sensitive = false;
		lendItemButton.Sensitive = false;
		editItemButton.Sensitive = false;

		lendItemButton.Clicked += LendOrReturnItem;
		editItemButton.Clicked += EditItem;

		// show all the widgets
		RestoreWindowState ();
		cairoShelf = new CairoShelf();
		swCairoShelf.AddWithViewport(cairoShelf);
		mcatalogMainWindow.ShowAll();

		// add some of the handles to the cairo shelf
		cairoShelf.Catalog = activeCatalog;
		cairoShelf.Presentation = presentation;

		cairoShelf.OnItemSelected += OnItemSelectionChanged;
// 		// cairoShelf.OnLendItemRequest += LendItem;
// 		// cairoShelf.OnReturnItemRequest += ReturnItem;
		cairoShelf.OnEditItemRequest += EditItem;
// 		// cairoShelf.OnNewItemRequest += AddItem;
// 		// cairoShelf.OnItemDeleted += DeleteItems;

		program.Run();
	}

	public void PopulateCatalogs ()
	{
		catalogList = new CatalogList();
		catalogList.Selection.Changed += OnCatalogActivated;

		swCatalogs.Add (catalogList);

		int i=0;
		int sel = 0;
		string active = Conf.Get("active_catalog", "books");
		foreach (Catalog catalog in database.Catalogs()) {
			catalogList.AddCatalog (catalog);
			catalog.LoadAll();
			if (catalog.Name.Equals(active)) {
				sel = i;
			}
			i++;
		}    
		catalogList.SetSelectedCatalog(sel);
		/*System.Console.WriteLine("initialize presentation....");
		presentation.Init();
		Catalog newActiveCatalog = catalogList.GetSelectedCatalog();
		SetActiveCatalog(newActiveCatalog);*/
	}

	private void PopulateBorrowers ()
	{
		borrowerList = new BorrowerList (database);
		borrowerList.OnBorrowerSelected += BorrowerSelected;
		swBorrowers.Add (borrowerList);	
	}

	private void OnAddBorrowerButtonClicked (object o, EventArgs args)
	{
		Console.WriteLine ("ADD");
	}

	private void OnRemoveBorrowerButtonClicked (object o, EventArgs args)
	{
		Console.WriteLine ("REMOVE");
	}

	private void OnPreferencesActivated (object o, EventArgs args)
	{
        new PreferencesDialog (cairoShelf);
	}

	// Change the view mode
	private void OnViewToggled (object o, EventArgs args)
	{
		ViewRadioMenuItemChanged((RadioMenuItem)o);
	}

	private void OnButtonShelfClicked (object o, EventArgs args)
	{
		if (buttonShelf.Active) {
			Console.WriteLine ("SETVIEW DESDE OnButtonShelfClicked");
			SetView (View.Shelf);
			buttonList.Active = false;
		}
	}

	private void OnButtonListClicked (object o, EventArgs args)
	{
		if (buttonList.Active) {
			Console.WriteLine ("SETVIEW DESDE OnButtonListClicked");
			SetView (View.List);
			buttonShelf.Active = false;
		}
	}

	private void ViewRadioMenuItemChanged (RadioMenuItem radio)
	{
		GLib.SList list = radio.Group;
		foreach (RadioMenuItem item in list) {
			if (item.Active) {
				if (activeView != (View)item.Data["view"]) {
					Console.WriteLine ("SETVIEW DESDE ViewRadioMenuItemChanged");
					SetView ((View)item.Data["view"]);
				}
				break;
			}
		}
	}

	private void SetView (View view)
	{
		SetView (view, false);
	}

	private void SetView (View view, bool force)
	{
		Console.WriteLine ("ACTIVANDO VISTA: "+view);
		if (activeView != view || force) {
			activeView = view;

			if (swItemsList.Child != null) {
				swItemsList.Remove (swItemsList.Child);
			}

			database.SetCatalogView (activeCatalog.Name, view);
			if (view == View.List) {
				itemList = database.LoadItemList(activeCatalog, presentation);
				itemList.OnItemSelected      += OnItemSelectionChanged;
				itemList.OnLendItemRequest   += LendItem;
				itemList.OnReturnItemRequest += ReturnItem;
				itemList.OnEditItemRequest   += EditItem;
				itemList.OnItemDeleted       += DeleteItems;

				swItemsList.Add (itemList);
				notebook.CurrentPage = 0;
				zoomInButton.Visible = false;
				zoomOutButton.Visible = false;
				menuItemListView.Activate();
				buttonList.Active = true;
			}
			if (view == View.Shelf) {
				addItemButton.Sensitive = true;
				notebook.CurrentPage = 1;
				zoomInButton.Visible = true;
				zoomOutButton.Visible = true;
				menuItemShelfView.Activate();
				buttonShelf.Active = true;
			}

			titleWidget.SetView (view);
			titleWidget.Table = activeCatalog.ShortDescription;
			titleWidget.Fields = database.GetColumns (activeCatalog.Table);
			titleWidget.Order = activeCatalog.Order;
		}
	}

	private void OnZoomInActivated (object o, EventArgs args)
	{
		cairoShelf.Zoom += 0.2;
	}

	private void OnZoomOutActivated (object o, EventArgs args)
	{
		if(cairoShelf.Zoom > 0.2) {
			cairoShelf.Zoom -= 0.2;
		}
	}

	private void OnNormalZoomActivated (object o, EventArgs args)
	{
		cairoShelf.Zoom = 1;
	}

	private void SetActiveCatalog (Catalog newActiveCatalog)
	{
		if (newActiveCatalog != null) {
			activeCatalog = newActiveCatalog;

			menuItemAddItem.Sensitive = true;
			addItemButton.Sensitive = true;
			Console.WriteLine ("SETVIEW DESDE OnCatalogActivated");
			SetView (database.GetCatalogView (activeCatalog.Name), true);

			activeCatalog.LoadAll();
			if (cairoShelf != null)
				cairoShelf.Catalog = activeCatalog;

			Conf.Set("active_catalog", activeCatalog.Name);
			if (borrowerList!=null) {
				borrowerList.Selection.UnselectAll ();
			}
		}
		else {
			menuItemAddItem.Sensitive = false;
			addItemButton.Sensitive = false;
			titleWidget.Table = "";
		}
	}

	// Event fired when a catalog has been selected
	private void OnCatalogActivated (object o, EventArgs args)
	{
		#if (DEBUG)
		System.Console.WriteLine("Main.cs (OnCatalogActivated)");
		#endif
		presentation.Init();
		Catalog newActiveCatalog = catalogList.GetSelectedCatalog();
		SetActiveCatalog(newActiveCatalog);
		lendItemButton.Sensitive = false;
		editItemButton.Sensitive = false;
		addItemButton.Sensitive = true;
		removeItemButton.Sensitive = false;
		#if (DEBUG)
		System.Console.WriteLine("Main.cs (OnCatalogActivated): end");
		#endif
	}

	private void BorrowerSelected (object o, EventArgs args)
	{
		Borrower borrower = (Borrower)o;
		activeCatalog.ItemCollection.LoadBorrowerList(borrower);
		string title = String.Format (Mono.Unix.Catalog.GetString ("{0} borrower by {1}"),activeCatalog.ShortDescription, borrower.Name);
		titleWidget.Table = title; 
		catalogList.Selection.UnselectAll ();
		addItemButton.Sensitive=false;
	}

	private void OnOrderChanged (Order order)
	{
		activeCatalog.Order = order;
		if (activeView == View.Shelf) {
			try {
				cairoShelf.ForceRedraw();
			} catch {}
		}
	}

	private void BorrowerListChanged (object o, EventArgs args)
	{
		if (borrowerList.Count() == 0) {
			borrowersFrame.Visible = false;
		}
		else {
			borrowersFrame.Visible = true;
		}

		OnItemSelectionChanged (o, args);
	}

	// called when we switch what individual item is selected
	private void OnItemSelectionChanged (object o, EventArgs args)
	{
		#if (DEBUG)
		System.Console.WriteLine("Main.cs (OnItemSelectionChanged): method invoked");
		#endif
		ArrayList items = activeCatalog.ItemCollection.GetSelectedItems();
		if (items.Count > 0) {
			menuItemRemoveItem.Sensitive = true;
			removeItemButton.Sensitive = true;

			if (items.Count == 1) {
				lendItemButton.Sensitive = true;
				editItemButton.Sensitive = true;

				Item selectedItem = (Item)items[0];
				if (database.IsBorrowed (selectedItem)) {
					lendItemButton.Label = Mono.Unix.Catalog.GetString ("Return");
				}
				else {
					lendItemButton.Label = Mono.Unix.Catalog.GetString ("Lend");
				}
			}
			else {
				lendItemButton.Sensitive = false;
				editItemButton.Sensitive = false;
			}
		}
		else {
			menuItemRemoveItem.Sensitive = false;
			removeItemButton.Sensitive = false;
			lendItemButton.Sensitive = false;
			editItemButton.Sensitive = false;
		}
	}

	// Add a new item to the active catalog
	private void OnAddItemToolbarButtonClicked ()
	{
		AddItem (null, null);
	}

	private void OnAddItemButtonClicked (object o, EventArgs args)
	{
		AddItem (null, null);
	}

	private void AddItem (object o, EventArgs args)
	{
		activeCatalog.OpenAddItemDialog();
	}

	private void EditItem (object o, EventArgs args)
	{
		activeCatalog.EditSelectedItem();
	}

	/// <summary>Called when the Lend/Return button is called</summary>
	private void LendOrReturnItem (object o, EventArgs args)
	{
		ArrayList items = activeCatalog.ItemCollection.GetSelectedItems();
		if (items.Count > 0) {
			Item selectedItem = (Item)items[0];
			if (database.IsBorrowed (selectedItem)) {
				ReturnItem (null, null);
			}
			else {
				LendItem (null, null);
			}
		}
	}

	/// <summary>Sets an item as being Lended</summary>
	private void LendItem (object o, EventArgs args)
	{
		activeCatalog.LendSelectedItem();
	}

	/// <summary>Sets an item as being returned</summary>
	private void ReturnItem (object o, EventArgs args)
	{
		activeCatalog.ReturnSelectedItem();
		if (activeView == View.Shelf)
			cairoShelf.ForceRedraw();
	}

	// Delete the active item
	private void OnDeleteItemToolbarButtonClicked ()
	{
		DeleteItems (null, null);
	}

	private void OnDeleteItemButtonClicked (object o, EventArgs args)
	{
		DeleteItems (null, null); 
	}

	private void DeleteItems (object o, EventArgs args)
	{
		Gtk.Dialog dialog = new MessageDialog (null,
				DialogFlags.Modal,
				MessageType.Question,
				ButtonsType.OkCancel,
				Mono.Unix.Catalog.GetString ("Remove the selected items?"));
		ResponseType response = (ResponseType)dialog.Run();

		if (response == ResponseType.Ok) {
			ArrayList selected = activeCatalog.ItemCollection.GetSelectedItems ();
			foreach (Item item in selected) {
				if (database.IsBorrowed (item)) {
					database.ReturnItem (item);
					activeCatalog.BorrowerListChanged ();
				}
				activeCatalog.RemoveItem (item);
			}
		}
		if (activeView == View.Shelf) {
			cairoShelf.ForceRedraw();
		}
		dialog.Destroy();
	}

	private void OnExportActivate (object o, EventArgs args)
	{
		new ExportDialog (activeCatalog, searchOn);
	}

	private void OnSearchEntryActivated (object o, EventArgs args)
	{
		catalogList.Search (searchEntry.Text);
		searchOn = searchEntry.Text.Equals ("");
	}

	private void OnQuitActivateEvent (object o, EventArgs args)
	{
		Exit ();
	}

	private void OnWindowDeleteEvent (object o, DeleteEventArgs args) 
	{
		Exit ();
		args.RetVal = true;
	}

	private void OnItemListPanedResized (object o, SizeRequestedArgs args)
	{
		Gdk.Rectangle rect = ((Paned)o).Child1.Allocation;
		// itemShelf.SetSizeRequest (rect.Width, rect.Height);
        cairoShelf.ForceReallocate();
		rect = ((Paned)o).Child2.Allocation;
		// swPresentation.SetSizeRequest(rect.Width, rect.Height);
		presentation.SetSizeRequest(rect.Width, rect.Height);
        // tell it to redraw
        presentation.ForceRedraw();
	}

	// just go and restore the window size and what not with what
	// they were last saved at
	private void RestoreWindowState()
	{
		#if (DEBUG)
		System.Console.WriteLine("Main.cs(RestoreWindowState): start");
		#endif
		int width, height;

		width = Conf.Get ("ui/main_window_width", 600);
		height = Conf.Get("ui/main_window_height", 400);

		if (width == Int32.MaxValue) {
			mcatalogMainWindow.Maximize();
		}
		else {
			mcatalogMainWindow.Resize(width, height);
		}

		width = Conf.Get ("ui/catalog_list_width", 130);
		catalogListPaned.Position = width;

		width = Conf.Get ("ui/item_list_width", 360);
		itemListPaned.Position = width;

		height = Conf.Get ("ui/borrowers_list_height", 100);
		borrowersPaned.Position = height;

		if (borrowerList.Count() == 0) {
			borrowersFrame.Visible = false;
		}

		mcatalogMainWindow.WindowPosition = WindowPosition.CenterAlways;
		SetView (View.Shelf, true);
		#if (DEBUG)
		System.Console.WriteLine("Main.cs(RestoreWindowState): end");
		#endif
	}

	private void SaveWindowState()
	{
		int height, width;

		if (mcatalogMainWindow.GdkWindow.State == Gdk.WindowState.Maximized) {
			Conf.Set("ui/main_window_width", Int32.MaxValue);
		}
		else {
			mcatalogMainWindow.GetSize(out width, out height);
			Conf.Set("ui/main_window_width", width);
			Conf.Set("ui/main_window_height", height);
		}

		Conf.Set("ui/catalog_list_width", catalogListPaned.Position);
		Conf.Set("ui/item_list_width", itemListPaned.Position);
		Conf.Set ("ui/borrowers_list_height", borrowersPaned.Position);

		Conf.Sync ();
	}

	private void ShowAboutDialog (System.Object o, EventArgs e)
	{
		MCatalogAboutDialog about = new MCatalogAboutDialog();
		about.Run();
		about.Destroy();
	}

	private void Exit ()
	{
		SaveWindowState();
		database.Close();
		program.Quit();
	}
}
}
