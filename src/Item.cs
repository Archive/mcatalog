/*
 * Copyright (C) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using Gdk;
using Cairo;
using Gtk;
namespace MCatalog {
public class Item
{
	private Database database;
	
	private int id;
	private string table;
	private string cover;
	private bool hasCover;
	private bool selected;
	private Gdk.Rectangle rect;
	private Gdk.Pixbuf pb;
	private int row;
	private int col;
	private double scaleFactor = 1.0;

	public Item (Database database, string table, int id):
		this (database, table, id, true)
	{
	}
	
	public Item (Database database, string table, int id, bool isNew)
	{
		this.database = database;
		this.id = id;
		this.table = table;

		cover = database.GetItemCover (table, id);
		
		if (cover != null) {
			hasCover = true;
		}
		else {
			cover = Defines.IMAGE_DATADIR + "/shelfbgnp.png";
			hasCover = false;
		}
		Gtk.Image img = new Gtk.Image(cover);
		this.pb = img.Pixbuf;
	}

	public Hashtable GetInfo () {
		return database.GetItemInfo (table, id);
	}

	public bool IsSelected {
		get {
			return selected;
		}
		set {
			selected = value;
		}
	}
	
	public int Id {
		get {
			return id;
		}
	}

	public string Table {
		get {
			return this.table;
		}
	}

	public string Cover {
		get {
			return cover;
		}
	}

	public static void RemoveCache (string table, int id)
	{
		try {
			StringBuilder cacheDir = new StringBuilder (300);
			cacheDir = cacheDir.Append (Conf.HomeDir);
			
			StringBuilder file = new StringBuilder (100);
			file = file.Append (table);
			file = file.Append (id);
			file = file.Append (".png");

			StringBuilder aux;
			
			aux = cacheDir.Append ("/cache/large/");
			aux = aux.Append (file.ToString());
			System.Console.WriteLine("Item.cs(RemoveCache): deleteing file " + aux.ToString());
			File.Delete (aux.ToString());
		}
		catch {}
	}

	public Gdk.Rectangle Rect {
		get {
			return rect;
		}
		set {
			rect = value;
		}
	}

	public Gdk.Pixbuf Pixbuf {
		get {
			return pb;
		}
	}

	public int Row {
		get {
			return row;
		}
		set {
			row = value;
		}
	}

	public int Col {
		get {
			return col;
		}
		set {
			col = value;
		}
	}

	public double ScaleFactor {
		get {
			return scaleFactor;
		}
		set {
			scaleFactor = value;
		}
	}
}
}
