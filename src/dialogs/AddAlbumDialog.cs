/*
 * Copyright (C) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;

using Gdk;
using Gtk;
using Gnome;

using Mono.Posix;

using Amazon;

namespace MCatalog {
public class AddAlbumDialog: AddDialog
{
	[Glade.Widget] private Gtk.Entry entryArtists;
	[Glade.Widget] private Gtk.Entry entryLabel;
	[Glade.Widget] private Gtk.TextView textviewTracks;
	[Glade.Widget] private Gtk.Entry entryDate;
	[Glade.Widget] private Gtk.Entry entryRuntime;
	[Glade.Widget] private Gtk.Entry entryStyle;
	[Glade.Widget] private Gtk.Entry entryAsin;
	[Glade.Widget] private Gtk.Entry entryMedium;
	[Glade.Widget] private Gtk.TextView textviewComments;

	public AddAlbumDialog (Catalog catalog) : base (catalog,
							"albumDialogWidgetsTable",
							Mono.Unix.Catalog.GetString ("Add a new Album"),
							"albums")
	{
	}

	public override void EditItem (Item item)
	{
		this.Title = Mono.Unix.Catalog.GetString ("Edit");
		this.Update = item.Id;

		Hashtable itemInfo = item.GetInfo ();

		if (itemInfo["image"] != null) {
			imageChooserButton.SetFilename(itemInfo["image"].ToString());
		}

		ratingWidget.Value = Int32.Parse (itemInfo["rating"]!=null?itemInfo["rating"].ToString():"1");
		entryTitle.Text = itemInfo["title"]!=null?itemInfo["title"].ToString():"";
		entryArtists.Text = itemInfo["author"]!=null?itemInfo["author"].ToString():"";
		entryLabel.Text = itemInfo["label"]!=null?itemInfo["label"].ToString():"";
		entryDate.Text = itemInfo["date"]!=null?itemInfo["date"].ToString():"";
		entryStyle.Text = itemInfo["style"]!=null?itemInfo["style"].ToString():"";
		entryAsin.Text = itemInfo["asin"]!=null?itemInfo["asin"].ToString():"";
		textviewTracks.Buffer.Text = itemInfo["tracks"]!=null?itemInfo["tracks"].ToString():"";
		entryMedium.Text = itemInfo["medium"]!=null?itemInfo["medium"].ToString():"";
		entryRuntime.Text = itemInfo["runtime"]!=null?itemInfo["runtime"].ToString():"";
		textviewComments.Buffer.Text = itemInfo["comments"]!=null?itemInfo["comments"].ToString():"";
	}
	
	protected override void FillDialogFromSearch (SearchResults genericResults)
	{
		SearchResultsAlbum results = (SearchResultsAlbum) genericResults;

		if (results.Image != null) {
			string prefix = results.Name!=null?results.Name:"album-";
			imageChooserButton.SetFilename(GetImage(results.Image, "album-"+prefix+"-"));
		}
		
		entryTitle.Text = results.Name!=null?results.Name:"";
		
		entryArtists.Text = "";
		if (results.Artists != null) {
			for (int i = 0; i < results.Artists.Length; i++) {
				if (i > 0) entryArtists.Text += ", ";
				entryArtists.Text += results.Artists[i];
			}
		}
		
		entryLabel.Text = results.Label!=null?results.Label:"";
		
		textviewTracks.Buffer.Text = "";
		if (results.Tracks != null) {
			for (int i = 0; i < results.Tracks.Count; i++) {
				textviewTracks.Buffer.Text += (string) results.Tracks[i];
				if (i+1 < results.Tracks.Count) {
					textviewTracks.Buffer.Text += "\n";
				}
			}
		}

		entryDate.Text = results.Date!=null?results.Date:"";
		entryRuntime.Text = results.Runtime!=null?results.Runtime:"";
		entryStyle.Text = results.Style;
		entryAsin.Text = results.ASIN!=null?results.ASIN:""; 
		entryMedium.Text = results.Medium;
		textviewComments.Buffer.Text = results.Comments!=null?results.Comments:"";
	}
	
	public override void OnOkButtonClicked (object o, EventArgs args)
	{
		Hashtable columns = new Hashtable ();
		if (this.Update == -1) { 
			columns.Add ("id", null);
		}
		else {
			columns.Add ("id", this.Update.ToString());
		}
		
		if (imageChooserButton.Filename != null) {
			FileInfo fileInfo = new FileInfo (imageChooserButton.Filename);
			if (fileInfo.Length == 807) {
				columns.Add ("image", null);
			}
			else {
				columns.Add ("image", imageChooserButton.Filename);
			}
		}
		else {
			columns.Add ("image", null);
		}
		
		columns.Add ("rating", ratingWidget.Value.ToString());
		columns.Add ("title", entryTitle.Text);
		columns.Add ("author", entryArtists.Text);
		columns.Add ("label", entryLabel.Text);
		columns.Add ("date", entryDate.Text);
		columns.Add ("style", entryStyle.Text);
		columns.Add ("asin", entryAsin.Text);
		columns.Add ("tracks", textviewTracks.Buffer.Text);
		columns.Add ("medium", entryMedium.Text);
		columns.Add ("runtime", entryRuntime.Text);
		columns.Add ("comments", textviewComments.Buffer.Text);
		
		if (this.Update == -1) {
			catalog.NewItem (columns);
		}
		else {
			catalog.UpdateItem (this.Update, columns);
		}
		
		this.Destroy();
	}

}
}
