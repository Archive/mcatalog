/*
 * Copyright (C) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more results.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;

using Gdk;
using Gtk;
using Gnome;

using Mono.Posix;

using Amazon;

namespace MCatalog {
public class AddBookDialog: AddDialog
{
	[Glade.Widget] private Gtk.Entry entryOriginalTitle;
	[Glade.Widget] private Gtk.Entry entryAuthor;
	[Glade.Widget] private Gtk.Entry entryDate;
	[Glade.Widget] private Gtk.Entry entryPages;
	[Glade.Widget] private Gtk.Entry entryGenre;
	[Glade.Widget] private Gtk.Entry entryISBN;
	[Glade.Widget] private Gtk.Entry entryPublisher;
	[Glade.Widget] private Gtk.Entry entryCountry;
	[Glade.Widget] private Gtk.Entry entryLanguage;
	[Glade.Widget] private Gtk.TextView textviewComments;

	public AddBookDialog (Catalog catalog) : base (catalog,
						       "bookDialogWidgetsTable",
						       Mono.Unix.Catalog.GetString ("Add a new book"),
						       "books")
	{
	}

	public override void EditItem (Item item)
	{
		this.Title = Mono.Unix.Catalog.GetString ("Editing");
		this.Update = item.Id;
		Hashtable itemInfo = item.GetInfo ();

		if (itemInfo["image"] != null) {
			imageChooserButton.SetFilename(itemInfo["image"].ToString());
		}

		entryTitle.Text = itemInfo["title"]!=null?itemInfo["title"].ToString():"";
		entryOriginalTitle.Text = itemInfo["original_title"]!=null?itemInfo["original_title"].ToString():"";
		entryAuthor.Text = itemInfo["author"]!=null?itemInfo["author"].ToString():"";
		entryDate.Text = itemInfo["date"]!=null?itemInfo["date"].ToString():"";
		entryGenre.Text = itemInfo["genre"]!=null?itemInfo["genre"].ToString():"";
		entryPages.Text = itemInfo["pages"]!=null?itemInfo["pages"].ToString():"";
		entryPublisher.Text = itemInfo["publisher"]!=null?itemInfo["publisher"].ToString():"";
		entryISBN.Text = itemInfo["isbn"]!=null?itemInfo["isbn"].ToString():"";
		entryCountry.Text = itemInfo["country"]!=null?itemInfo["country"].ToString():"";
		entryLanguage.Text = itemInfo["language"]!=null?itemInfo["language"].ToString():"";
		textviewComments.Buffer.Text = itemInfo["comments"]!=null?itemInfo["comments"].ToString():"";
		ratingWidget.Value = Int32.Parse (itemInfo["rating"]!=null?itemInfo["rating"].ToString():"1");
	}
	
	protected override void FillDialogFromSearch (SearchResults genericResults)
	{
		SearchResultsBook results = (SearchResultsBook)genericResults;
		if (results.Image != null) {
			string prefix = results.Name!=null?results.Name:"book-";
			imageChooserButton.SetFilename(GetImage(results.Image, "book-"+prefix+"-"));
		}

		entryTitle.Text = results.Name!=null?results.Name:"";
		
		entryAuthor.Text = "";
		for (int i = 0; i < results.Authors.Length; i++) {
			entryAuthor.Text += results.Authors[i];
			if (i+1 < results.Authors.Length) {
				entryAuthor.Text += ", ";
			}
		}
		
		entryDate.Text = results.Date!=null?results.Date:"";
		entryPages.Text = results.Pages!=null?results.Pages:"";
		entryISBN.Text = results.ISBN!=null?results.ISBN:"";
		entryPublisher.Text = results.Publisher!=null?results.Publisher:"";
		textviewComments.Buffer.Text = results.Comments!=null?results.Comments:"";
	}
	
	public override void OnOkButtonClicked (object o, EventArgs args)
	{
		Hashtable columns = new Hashtable ();
		if (this.Update == -1) { 
			columns.Add ("id", null);
		}
		else {
			columns.Add ("id", this.Update.ToString());
		}
		
		if (imageChooserButton.Filename != null) {
			FileInfo fileInfo = new FileInfo (imageChooserButton.Filename);
			if (fileInfo.Length == 807) {
				columns.Add ("image", null);
			}
			else {
				columns.Add ("image", imageChooserButton.Filename);
			}
		}
		else {
			columns.Add ("image", null);
		}
		
		columns.Add ("rating", ratingWidget.Value.ToString());
		columns.Add ("title", entryTitle.Text);
		columns.Add ("original_title", entryOriginalTitle.Text);
		columns.Add ("author", entryAuthor.Text);
		columns.Add ("date", entryDate.Text);
		columns.Add ("genre", entryGenre.Text);
		columns.Add ("pages", entryPages.Text);
		columns.Add ("publisher", entryPublisher.Text);
		columns.Add ("isbn", entryISBN.Text);
		columns.Add ("country", entryCountry.Text);
		columns.Add ("language", entryLanguage.Text);
		columns.Add ("comments", textviewComments.Buffer.Text);
		
		if (this.Update == -1) {
			catalog.NewItem (columns);
		}
		else {
			catalog.UpdateItem (this.Update, columns);
		}
		
		this.Destroy();
	}
}
}
