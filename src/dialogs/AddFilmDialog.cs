/*
 * Copyright (C) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more results.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;

using Gdk;
using Gtk;
using Gnome;

using Mono.Posix;

using Amazon;

namespace MCatalog {
public class AddFilmDialog: AddDialog
{
	[Glade.Widget] private Gtk.Entry entryOriginalTitle;
	[Glade.Widget] private Gtk.Entry entryDirector;
	[Glade.Widget] private Gtk.Entry entryDate;
	[Glade.Widget] private Gtk.Entry entryRuntime;
	[Glade.Widget] private Gtk.Entry entryGenre;
	[Glade.Widget] private Gtk.Entry entryDistributor;
	[Glade.Widget] private Gtk.Entry entryMedium;
	[Glade.Widget] private Gtk.Entry entryCountry;
	[Glade.Widget] private Gtk.Entry entryLanguage;
	[Glade.Widget] private Gtk.TextView textviewComments;
	[Glade.Widget] private Gtk.TextView textviewStarring;

	public AddFilmDialog (Catalog catalog) : base (catalog,
	                           "filmDialogWidgetsTable",
	                           Mono.Unix.Catalog.GetString ("Add a new film"),
	                           "films")
	{
	}

	public override void EditItem (Item item)
	{
		this.Title = Mono.Unix.Catalog.GetString ("Edit");
		this.Update = item.Id;

		Hashtable itemInfo = item.GetInfo ();

		if (itemInfo["image"] != null) {
			#if (DEBUG)
			System.Console.WriteLine("dialog/AddFilmDialog.cs (EditItem): setting image to {0}", itemInfo["image"].ToString());
			#endif
			imageChooserButton.SetFilename(itemInfo["image"].ToString());
		} else {
			#if (DEBUG)
			System.Console.WriteLine("dialog/AddFilmDialog.cs (EditItem): not setting image -- it is null!");
			#endif
		}

		ratingWidget.Value = Int32.Parse (itemInfo["rating"]!=null?itemInfo["rating"].ToString():"1");
		entryTitle.Text = itemInfo["title"]!=null?itemInfo["title"].ToString():"";
		entryOriginalTitle.Text = itemInfo["original_title"]!=null?itemInfo["original_title"].ToString():"";
		entryDirector.Text = itemInfo["director"]!=null?itemInfo["director"].ToString():"";
		textviewStarring.Buffer.Text = itemInfo["starring"]!=null?itemInfo["starring"].ToString():"";
		entryDate.Text = itemInfo["date"]!=null?itemInfo["date"].ToString():"";
		entryGenre.Text = itemInfo["genre"]!=null?itemInfo["genre"].ToString():"";
		entryRuntime.Text = itemInfo["runtime"]!=null?itemInfo["runtime"].ToString():"";
		entryCountry.Text = itemInfo["country"]!=null?itemInfo["country"].ToString():"";
		entryLanguage.Text = itemInfo["language"]!=null?itemInfo["language"].ToString():"";
		entryDistributor.Text = itemInfo["distributor"]!=null?itemInfo["distributor"].ToString():"";
		entryMedium.Text = itemInfo["medium"]!=null?itemInfo["medium"].ToString():"";
		textviewComments.Buffer.Text = itemInfo["comments"]!=null?itemInfo["comments"].ToString():"";
	}
	
	protected override void FillDialogFromSearch (SearchResults genericResults) 
	{
		SearchResultsFilm results = (SearchResultsFilm)genericResults;
		
		if (results.Image != null) {
			string prefix = results.Name!=null?results.Name:"film-";
			imageChooserButton.SetFilename(GetImage(results.Image, "film-"+prefix+"-"));
		}
		ratingWidget.Value = results.Rating;
		
		entryTitle.Text = results.Name!=null?results.Name:"";
		
		entryDirector.Text = "";
		if (results.Directors != null) {
			for (int i = 0; i < results.Directors.Length; i++) {
				if (i > 0) entryDirector.Text += ", ";
				entryDirector.Text += results.Directors[i];
			}
		}
		
		if (results.Date != null) {
			entryDate.Text = results.Date.Substring (0, 4);
		}
		
		entryGenre.Text = results.Genre;
		entryRuntime.Text = results.RunningTime!=null?results.RunningTime:"";
		entryCountry.Text = results.Country; 
		entryLanguage.Text = results.Language;
		entryDistributor.Text = results.Manufacturer!=null?results.Manufacturer:"";
		entryMedium.Text = results.Medium;
		
		if (results.Comments != null) {
			textviewComments.Buffer.Text = results.Comments;
		}
		
		textviewStarring.Buffer.Text = "";
		if (results.Starring != null) {
			for (int i = 0; i < results.Starring.Length; i++) {
				textviewStarring.Buffer.Text += results.Starring[i];
				if (i+1 < results.Starring.Length) {
					textviewStarring.Buffer.Text += "\n";
				}
			}
		}
	}
	
	public override void OnOkButtonClicked (object o, EventArgs args)
	{
		Hashtable columns = new Hashtable ();
		if (this.Update == -1) { 
			columns.Add ("id", null);
		}
		else {
			columns.Add ("id", this.Update.ToString());
		}
		
		if (imageChooserButton.Filename != null) {
			FileInfo fileInfo = new FileInfo (imageChooserButton.Filename);
			if (fileInfo.Length == 807) {
				columns.Add ("image", null);
			}
			else {
				columns.Add ("image", imageChooserButton.Filename);
			}
		}
		else {
			columns.Add ("image", null);
		}
		
		columns.Add ("rating", ratingWidget.Value.ToString());
		columns.Add ("title", entryTitle.Text);
		columns.Add ("original_title", entryOriginalTitle.Text);
		columns.Add ("director", entryDirector.Text);
		columns.Add ("starring", textviewStarring.Buffer.Text);
		columns.Add ("date", entryDate.Text);
		columns.Add ("genre", entryGenre.Text);
		columns.Add ("runtime", entryRuntime.Text);
		columns.Add ("country", entryCountry.Text);
		columns.Add ("language", entryLanguage.Text);
		columns.Add ("distributor", entryDistributor.Text);
		columns.Add ("medium", entryMedium.Text);
		columns.Add ("comments", textviewComments.Buffer.Text);

		if (this.Update == -1) {
			catalog.NewItem (columns);
		}
		else {
			catalog.UpdateItem (this.Update, columns);
		}
		
		this.Destroy();
	}

	/*
	public static Cairo.Context CreateDrawable (Gdk.Drawable drawable)
	{
		Cairo.Context g = new Cairo.Context(gdk_cairo_create(drawable.Handle));
		if (g == null)
			throw new Exception("Couldn't create Cairo Context");
		return g;
	}

	/// <summary>
	/// draw the image in the cairo area
	/// </summary>
	private void OnExposed(object o, ExposeEventArgs args)
	{
		System.Console.WriteLine("on exposed called");
		Gdk.Rectangle rect = entryImageArea.Allocation;
		Cairo.Context c = CreateDrawable(entryImageArea.GdkWindow);
		c.Save();
		c.Rectangle(0, 0, rect.Width, rect.Height);
		c.Color = new Cairo.Color(0,0,0);
		c.Fill();
		c.Restore();
		if (entryImagePixbuf == null) {
			return;
		}
		double widthRatio;
		double heightRatio;
		int newX;
		int newY;
		widthRatio = (double) rect.Width / (double) entryImagePixbuf.Width;
		heightRatio = (double) rect.Height / (double) entryImagePixbuf.Height;
		if (widthRatio < heightRatio)
			heightRatio = widthRatio;
		else
			widthRatio = heightRatio;

		// figure out the position to center it
		newX = (rect.Width - (int)(entryImagePixbuf.Width*widthRatio))/2;
		newY = (rect.Height - (int)(entryImagePixbuf.Height*heightRatio))/2;
		Cairo.Matrix m = new Cairo.Matrix();
		m.InitTranslate(newX, newY);
		m.Scale(widthRatio, heightRatio);
		c.Save();
		c.Rectangle(0,0,rect.Width,rect.Height);
		c.Transform(m);
		Gdk.CairoHelper.SetSourcePixbuf(c, entryImagePixbuf, 0, 0);
		c.Fill();
		c.Restore();

		((IDisposable) c.Target).Dispose();
		((IDisposable) c).Dispose();
	} */

}
}
