using System;
using System.IO;

using Gtk;
using Glade;

using Mono.Posix;

namespace MCatalog {
public class PreferencesDialog: Dialog
{
	TreeStore themeStore;
	CairoShelf cairoShelf;

	public PreferencesDialog(CairoShelf cairoShelf): base ()
	{
		this.cairoShelf = cairoShelf;

		this.Title = Mono.Unix.Catalog.GetString ("Preferences");
		this.HasSeparator = false;
		this.SetDefaultSize (300, 200);

		Notebook notebook = new Notebook ();

		Glade.XML gxml = new Glade.XML (null, "mainwindow.glade", "themeSelectionHbox", null);
		HBox hBox = (HBox)gxml["themeSelectionHbox"];
		ScrolledWindow scrolledwindow = (ScrolledWindow)gxml["scrolledwindow1"];
		TreeView themeTreeview = CreateThemeTreeView ();
		themeTreeview.Selection.Changed += OnThemeTreeViewSelectionChanged;

		scrolledwindow.Add (themeTreeview);
		
		notebook.AppendPage (hBox, new Label (Mono.Unix.Catalog.GetString ("Theme")));

		this.VBox.Add (notebook);
		
		Button closeButton = (Button)this.AddButton (Gtk.Stock.Close, 1);
		closeButton.Clicked += OnCloseButtonClicked;
		
		this.ShowAll();
	}
	
	private TreeView CreateThemeTreeView ()
	{
		PopulateStore ();
		TreeView tv = new TreeView (themeStore);
		tv.HeadersVisible = false;
		CellRendererText ct = new CellRendererText ();
		tv.AppendColumn (Mono.Unix.Catalog.GetString ("Theme"), ct, new TreeCellDataFunc (CellDataFunc));

		tv.Visible = true;
		
		return tv;
	}

	private void PopulateStore ()
	{
		themeStore = new TreeStore (typeof(string));
                foreach (string dir in Directory.GetDirectories (Defines.IMAGE_DATADIR)) {
                        themeStore.AppendValues (new DirectoryInfo(dir).Name);
                }
	}
	
	private void OnThemeTreeViewSelectionChanged (object o, EventArgs args)
	{
		TreeIter iter;
		TreeModel model;

		if (((TreeSelection)o).GetSelected (out model, out iter))
		{
			ShelfTheme s = new ShelfTheme((string) model.GetValue(iter, 0));
			cairoShelf.Theme = s;
		}
	}
		
	private void CellDataFunc (Gtk.TreeViewColumn tree_column,
					Gtk.CellRenderer cell,
					Gtk.TreeModel tree_model,
					Gtk.TreeIter iter)
        {
                string val = (string) themeStore.GetValue (iter, 0);
                ((CellRendererText) cell).Text = val;
        }
	
	private void OnCloseButtonClicked (object o, EventArgs args)
	{
		this.Destroy();
	}
}
}
