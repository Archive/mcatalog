using System;
using Gtk;
using Mono.Unix;
using Gdk;
namespace MCatalog
{
	public class MCatalogAboutDialog : AboutDialog
	{
		private static string [] authors = {
			"Cesar Garcia Tapia",
			"Patrick Wagstrom"
		};

		private static  Translator[] translators = {
			new Translator("Rostislav \"zbrox\" Raykov", "Bulgarian"),
			new Translator("Miloslav Trmac", "Czech"),
			new Translator("Hendrik Richter", "German"),
			new Translator("Adam Weinberger", "Canadian English"),
			new Translator("Christopher Orr", "British English"),
			new Translator("Jaime Iniesta Alemán", "Spanish"),
			new Translator("Ilkka Tuohela", "Finnish"),
			new Translator("Christophe Sauthier", "French"),
			new Translator("Török Gábor", "Hungarian"),
			new Translator("Fabio Bonelli", "Italian"),
			new Translator("Pawan Chitrakar, Kapil Timilsina", "Nepali"),
			new Translator("Wouter Bolsterlee, Tino Meinen", "Dutch"),
			new Translator("Raphael Higino", "Brazilian Portuguese"),
			new Translator("Steve Murphy", "Kinyarwanda"),
			new Translator("Christian Rose", "Swedish"),
			new Translator("Maxim Dziumanenko", "Ukranian"),
			new Translator("Clytie Siddall", "Vietnamese"),
			new Translator("Funda Wang", "Chinese")
		};

		public MCatalogAboutDialog() : base()
		{
			Array.Sort(authors);
			// FIXME: at some point we should use a banshee like system for themes and icons
			// SetWindowIcon(this,"mcatalog");

			Name = Mono.Unix.Catalog.GetString("MCatalog");
			Version = Defines.VERSION;
			Comments = Mono.Unix.Catalog.GetString("Media cataloger");
			Copyright = Mono.Unix.Catalog.GetString("Copyright (c) 2003-2005 Cesar Garcia Tapia\n" +
			                              "Copyright (c) 2006 Patrick Wagstrom"
			);

			Website = "http://www.mcatalog.net/";
			WebsiteLabel = Mono.Unix.Catalog.GetString("MCatalog Home Page");

			// FIXME: add in proper license
			// License = Resource.GetFileContents("COPYING");
			WrapLicense = true;
			Authors = authors;
			TranslatorCredits = Translator.ToString(translators);
			// FIXME: at some point, we'll want to put the license in the about box, ala banshee
			// License = Resource.GetFileContesnts("COPYING");

			SetUrlHook(delegate(AboutDialog dialog, string link) {
				Gnome.Url.Show(link);
			});
		}

		/* blatantly stolen from the Banshee code for right now */
        private class Translator : IComparable
        {
            public Translator(string name, string language)
            {
                Name = name;
                Language = language;
            }
            
            public string Name;
            public string Language;
        
            public override string ToString()
            {
                return String.Format("{0} ({1})", Name, Language);
            }
            
            public int CompareTo(object o)
            {
                if(!(o is Translator)) {
                    throw new ArgumentException("Object must be Translator");
                }    
                
                return -(o as Translator).Name.CompareTo(Name);
            }
            
            public static string ToString(Translator [] translators)
            {
                string str = String.Empty;
                Array.Sort(translators);
                
                for(int i = 0; i < translators.Length; i++) {
                    str += translators[i].ToString() + "\n";
                }
                
                return str;
            }
        }
 
	}
}


