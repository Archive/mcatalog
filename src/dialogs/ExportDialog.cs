/*
 * Copyright (C) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.IO;
using System.Collections;

using Gtk;
using Gnome;

using Amazon;

namespace MCatalog {
public class ExportDialog: Gtk.Dialog
{
	private Catalog catalog;
	private string template;
	private Hashtable templates;
	private bool searchOn;
	
	private Gtk.Button cancelButton;
	private Gtk.Button okButton;
	private string exportFilename;

	[Glade.Widget] private ComboBox exportTemplateComboBox;
	[Glade.Widget] private Image exportPreviewImage;

	[Glade.Widget] private RadioButton exportRadioButtonSearch;
	[Glade.Widget] private RadioButton exportRadioButtonActive;
	[Glade.Widget] private Button exportOutputFileButton;
	[Glade.Widget] private Label exportOutputFileLabel;

	public ExportDialog (Catalog catalog, bool searchOn):
		base (Mono.Unix.Catalog.GetString ("Export"), null, DialogFlags.NoSeparator | DialogFlags.Modal)
	{
		this.catalog = catalog;
		this.templates = new Hashtable ();
		this.searchOn = searchOn;
		this.exportFilename = null;

		Glade.XML gxml = new Glade.XML (null, "mainwindow.glade", "exportDialogHBox", null);
		gxml.Autoconnect(this);

		this.exportOutputFileLabel.Text = Mono.Unix.Catalog.GetString("Select Export Location");
		cancelButton = (Button)this.AddButton (Gtk.Stock.Cancel, 0);
		okButton     = (Button)this.AddButton (Gtk.Stock.Ok, 1);
		cancelButton.Clicked += OnCancelButtonClicked;
		okButton.Clicked     += OnOkButtonClicked;
		exportOutputFileButton.Clicked += OnExportOutputFileButtonClicked;

		VBox vBox = this.VBox;
		vBox.Add ((Box)gxml["exportDialogHBox"]);

		PopulateComboBox ();

		if (!searchOn) {
			exportRadioButtonSearch.Sensitive = false;
		}

		exportRadioButtonActive.Label = String.Format (Mono.Unix.Catalog.GetString ("Export the whole {0} catalog"),catalog.ShortDescription);

		this.ShowAll();
	}

	private void PopulateComboBox ()
	{
		ComboBox aux = ComboBox.NewText ();
		aux.Changed += ChangeComboBox;

		Box box = (Box) exportTemplateComboBox.Parent;
		box.Remove (exportTemplateComboBox);
		exportTemplateComboBox = aux;
		box.PackEnd (exportTemplateComboBox);
		FileInfo[] files = new DirectoryInfo(Defines.TEMPLATES_DATADIR).GetFiles("*.rc");
		foreach (FileInfo fileInfo in files)
		{
			StreamReader sr = fileInfo.OpenText();
			string line;
			string path = "";
			string templateName = "";
			char[] delimiter= {'='};
			while (true) {
				line = sr.ReadLine ();
				if (line == null)
					break;

				string[] split = line.Split (delimiter);
				if (split[0] == null || split[1] == null) 
					continue;

				switch (split[0].ToUpper()) {
					case "NAME":
						templateName = split[1];
						path = System.IO.Path.ChangeExtension (fileInfo.FullName, "html");
						break;
					case "CATALOG":
						if (catalog.Name.Equals (split[1])) {
							exportTemplateComboBox.AppendText (templateName);
							templates.Add (templateName, path);
						}
						break;
				}
			}
			sr.Close ();
		}
		exportTemplateComboBox.Active = 0;
	}

	private void ChangeComboBox (object o, EventArgs args)
	{
		TreeIter iter;

		if (exportTemplateComboBox.GetActiveIter (out iter)) {
			template = (string) templates[(string) exportTemplateComboBox.Model.GetValue (iter, 0)];
			string imageFileName = template.Replace (".html", ".png");
			if (File.Exists (imageFileName)) {
				if (exportPreviewImage != null) {
					exportPreviewImage.File = imageFileName;
				}
			}

		}
	}

	private void OnCancelButtonClicked (object o, EventArgs args)
	{
		this.Destroy();
	}

	private void OnOkButtonClicked (object o, EventArgs args)
	{
		Exporter exporter = new Exporter (catalog);
		exporter.Template = template;
		// TODO: Investigate if there is a better way to do this
		if (exportFilename == null) {
			Gtk.Dialog dialog = new MessageDialog (null,
					DialogFlags.Modal,
					MessageType.Error,
					ButtonsType.Close,
					Mono.Unix.Catalog.GetString ("Please Select A File To Export To First"));
			dialog.Run();
			dialog.Destroy ();
			return;
		}

		if (exporter.Export (exportFilename)) {
			Gtk.Dialog dialog = new MessageDialog (null,
					DialogFlags.Modal,
					MessageType.Info,
					ButtonsType.Ok,
					Mono.Unix.Catalog.GetString ("Catalog succesfully exported"));
			dialog.Run();

			dialog.Destroy ();
		}
		else {
			Gtk.Dialog dialog = new MessageDialog (null,
					DialogFlags.Modal,
					MessageType.Error,
					ButtonsType.Close,
					Mono.Unix.Catalog.GetString ("Some error while exporting the catalog"));
			dialog.Run();

			dialog.Destroy ();
		}
		
		this.Destroy();
	}

	private void OnExportOutputFileButtonClicked(object o, EventArgs args)
	{
		FileChooserDialog fc = new FileChooserDialog(Mono.Unix.Catalog.GetString("Select Export Location"),
		                                             this,
		                                             FileChooserAction.Save);
		fc.AddButton(Gtk.Stock.Cancel, ResponseType.Cancel);
		fc.AddButton(Gtk.Stock.Open, ResponseType.Ok);
		fc.SelectMultiple = false;
		fc.DefaultResponse = ResponseType.Ok;

		if (fc.Run() == (int)ResponseType.Ok) {
			System.Console.WriteLine("URI Selected: {0}", fc.Filename);
			this.exportFilename = fc.Filename;
			exportOutputFileLabel.Text = fc.Filename;
		}
		fc.Destroy();

	}

}
}
