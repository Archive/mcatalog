/*
 * Copyright (c) 2006 Patrick Wagstrom <pwagstro@andrew.cmu.edu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * ShelfTheme.cs contains theme definitions for the shelf view of MCatalog.
 *
 * I've tried to design this infrastructure so we're not strictly limited to
 * making the selections based on a shelf view, but instead we've got multiple
 * options related to it.
 */

using Cairo;
using Gdk;
using System.Collections;
using System;

namespace MCatalog {
/// <summary>
/// A ShelfTheme object contains holders and a few functions for drawing a
/// Cairo enabled shelf object.
/// </summary>
public class ShelfTheme : Theme
{
	protected ImageSurface shelfbg;         // the image behind the shelves
	protected SurfacePattern shelfbgPattern; // a surface pattern corresponding to the shelfbg
	protected ImageSurface shelf;           // actual image of the shelf
	protected SurfacePattern shelfPattern;  // a surface pattern corresponding to the shelf image
	protected ImageSurface borrowedImage;   // the icon showing if borrowed
	protected Pattern borrowedImagePattern; // pattern of borrowed image
	protected ImageSurface selectedShelfbg;

 	protected ArrayList shelfLocations;     // the locations of the items on the shelf
 	protected ArrayList shelfItems;         // the items on the shelf
 	protected Item selectedItem;
 	protected int lastSelectedItem = -1;

 	protected static int ELEMENT_SPACING = 15;
 	protected static int DEFAULT_ELEMENT_SIZE = 110;
 	protected int ELEMENT_SIZE=110;

	private bool animateActive = false;
	protected CairoShelf c;

	// event handlers
	public event EventHandler OnItemSelected;
	public event EventHandler OnEditItemRequest;

	/// <summary>
	/// Default constructor for simple themes
	/// </summary>
	/// <param name="theme">Name of the Theme</param>
	public ShelfTheme(string theme) : base(theme) {
		string themePath = Defines.IMAGE_DATADIR + "/" + theme;
		shelfbg = new ImageSurface(themePath + "/background.png");
		shelfbgPattern = new SurfacePattern(shelfbg);
		shelf = new ImageSurface(themePath + "/shelf.png");
		shelfPattern = new SurfacePattern(shelf);
		borrowedImage = new ImageSurface(Defines.IMAGE_DATADIR + "/borrowed.png");
		borrowedImagePattern = new Pattern(borrowedImage);
		// FIXME: right now this image is hardcoded in, in the future, we'll want people
		// to be able to customize this by theme
		selectedShelfbg = new ImageSurface(Defines.IMAGE_DATADIR + "/shelfbg.png");
	}

	void DrawBackground(Cairo.Context g, int width, int height)
	{
		shelfbgPattern.Extend = Cairo.Extend.Repeat;
		g.Save();
		g.Pattern = shelfbgPattern;
		g.Rectangle(0,0,width,height);
		g.Fill();
		g.Restore();
	}

	protected void DrawItems(Cairo.Context g, Catalog catalog) {
		#if (DEBUGSHELFTHEME)
		System.Console.WriteLine("ShelfTheme.cs (DrawItems): method invoked");
		#endif
		ItemCollection ic;
		try {
			ic = catalog.ItemCollection;
		} catch {
			#if (DEBUGSHELFTHEME)
			System.Console.WriteLine("ShelfTheme.cs (DrawItems): item collection is null");
			#endif
			return;
		}
		#if (DEBUGSHELFTHEME)
		System.Console.WriteLine("ShelfTheme.cs (DrawItems): point two");
		#endif
		ImageSurface img;
		Cairo.Matrix m;
		Pattern pattern;
		int ctr = 0;
		int shelfHeight = shelf.Height;

		shelfPattern.Extend = Cairo.Extend.Repeat;

		/* hopefully we don't need to recalculate posisionts for everything */
		// CalculatePositions();

		// draw each of the films
		foreach (Item element in ic) {
			// if the item is seleted - then put the border on it
			if (element.IsSelected && element.ScaleFactor <= 1) {
				Pattern p2 = new Pattern(selectedShelfbg);
				g.Save();
				int selWidth = element.Rect.Width + ELEMENT_SPACING*2;
				int selHeight = element.Rect.Height + ELEMENT_SPACING*2;
				double hScale = ((double) selectedShelfbg.Height)/((double) selHeight);
				double wScale = ((double) selectedShelfbg.Width)/((double) selWidth);
				m = new Cairo.Matrix();
				m.InitScale(wScale, hScale);
				m.Translate(-1*(element.Rect.X-ELEMENT_SPACING), -1*(element.Rect.Y-ELEMENT_SPACING));
				p2.Matrix = m;
				g.Pattern = p2;
				g.Rectangle(element.Rect.X-ELEMENT_SPACING, element.Rect.Y-ELEMENT_SPACING, selWidth, selHeight);
				g.Fill();
				g.Restore();
			}

			g.Save();
			double hscale = ((double)ELEMENT_SIZE)/((double) element.Pixbuf.Height);
			m = new Cairo.Matrix();
			m.InitTranslate(element.Rect.X, element.Rect.Y);
			m.Scale(hscale, hscale);
			g.Rectangle(element.Rect.X, element.Rect.Y, element.Pixbuf.Width*hscale, element.Pixbuf.Height*hscale);
			g.Transform(m);
			// FIXME: this is probably broken
			Gdk.CairoHelper.SetSourcePixbuf(g, element.Pixbuf, 0, 0);
			g.Fill();
			g.Restore();

			//  if the item is borrowed, draw the emblems
			if (catalog.IsBorrowed(element)) {
				DrawBorrowed(element, g);
			}
		}

		// finally, draw the shelves in the proper positions
		foreach (int yPos in shelfLocations) {
				g.Save();
				m = new Cairo.Matrix();
				m.InitTranslate(0, -1*(yPos));
				shelfPattern.Matrix = m;
				g.Pattern = shelfPattern;
				g.Rectangle(0, yPos, c.Allocation.Width, shelfHeight);
				g.Fill();
				g.Restore();
		}

		// actually, the real finally is to draw scaled elements...
		if (this.animateActive) {
			foreach (Item element in ic) {
				if (element.ScaleFactor > 1) {
					g.Save();
					double hscale = ((double) ELEMENT_SIZE)/((double) element.Pixbuf.Height);
					hscale = hscale * element.ScaleFactor;

					int inflateFactor = (int)((int) (ELEMENT_SIZE * element.ScaleFactor) - ELEMENT_SIZE)/2;
					Gdk.Rectangle r = element.Rect;
					r.X -= inflateFactor;
					r.Y -= inflateFactor;
					r.Width += 2*inflateFactor;
					r.Height += 2*inflateFactor;

					m = new Cairo.Matrix();
					m.InitTranslate(r.X, r.Y);
					m.Scale(hscale, hscale);
					g.Rectangle(r.X, r.Y, r.Width, r.Height);
					g.Transform(m);
					Gdk.CairoHelper.SetSourcePixbuf(g, element.Pixbuf, 0, 0);

					g.Fill();
					g.Restore();
				}
			}
		}
	}


	/// <summary>
	/// Basic drawing infrastructure
	/// </summary>
	/// <param name="g">Cairo.Context to draw on</param>
	/// <param name="c">handle </param>
	public override void Draw(Cairo.Context g, CairoShelf c) {
		#if (DEBUGSHELFTHEME)
		System.Console.WriteLine("ShelfTheme.cs (Draw): method invoked");
		#endif
		int x, y, w, h, d;
		c.GdkWindow.GetGeometry(out x, out y, out w, out h, out d);
		this.c = c;
		CalculatePositions(c.Catalog);
		DrawBackground(g, w, h);
		DrawItems(g, c.Catalog);
	}

	/// <summary>
	/// Handles button presses on the drawing them
	/// </summary>
	/// <param name="ev">Gdk.EventButton event</param>
	public override void ButtonPress(Gdk.EventButton ev) {
		#if (DEBUGSHELFTHEME)
		System.Console.WriteLine("ShelfTheme.cs (ButtonPress): method invoked");
		#endif

		// if we haven't set a catalog yet, we return
		if (c.Catalog == null)
			return;

		ItemCollection ic = c.Catalog.ItemCollection;
		Gdk.Point pt = new Gdk.Point((int)ev.X, (int)ev.Y);
		Gdk.Rectangle rect;
		bool itemSelected = false;

		// this theme only responds to button 1
		if (ev.Button != 1)
			return;
		c.GrabFocus();
		c.Catalog.ItemCollection.UnselectAll();
		foreach (Item element in ic) {
			rect = element.Rect;
			if (rect.Contains(pt)) {
				ic.UnselectAll();
				element.IsSelected = true;
				selectedItem = element;

				ActivateItemAnimation(selectedItem);
				// this indicates if we actually clicked on a selection or not
				this.OnItemSelected(selectedItem, null);
				
				// FIXME: this should also be handled by the CairoShelf method
				if (ev.Type == EventType.TwoButtonPress) {
					#if (DEBUGSHELFTHEME)
					System.Console.WriteLine("ShelfTheme.cs (ButtonPress): double clicked on item");
					#endif
					this.lastSelectedItem = element.Id;
					this.OnEditItemRequest(selectedItem, null);
				}
				itemSelected = true;
			} else {
				element.IsSelected = false;
				// Invoke a redraw on the entire layout because we've selected or unselected something
				// FIXME: in reality, we should just damage the area around the individual image that
				// we've selected or unselected
				c.ForceRedraw();
			}
		}
		if (itemSelected == false)
			c.ClearSelection();
	}

	/// <summary>
	/// Handles keyboard events for the widget.  This basically ignores
	/// everything except the arrow keys, space, and return.
	/// </summary>
	public bool KeyPress(Gdk.EventKey e)
	{
		#if (DEBUGSHELFTHEME)
		System.Console.WriteLine("ShelfTheme.cs (KeyPress): method invoked");
		#endif
		int row = 0;
		int col = 0;
		ArrayList al;
		Item i;
		if (e.Key.ToString() != "Left"  &&
			e.Key.ToString() != "Right"  && e.Key.ToString() != "Up"  &&
			e.Key.ToString() != "Down" && e.Key.ToString() != "space" &&
			e.Key.ToString() != "Return") {
				return false;
		}

		// catch the case when nothing is displayed
		if (shelfItems.Count == 0)
			return false;
		#if (DEBUGSHELFTHEME)
		System.Console.WriteLine("ShelfTheme.cs (Point Two): method invoked");
		#endif

		// selct the first item when nothing is selected
		if (selectedItem == null) {
			if (shelfItems != null && shelfItems.Count > 0) {
				al = (ArrayList) shelfItems[0];
				i = (Item) al[0];
				i.IsSelected = true;
				selectedItem = i;
				this.OnItemSelected(selectedItem, null);
				// c.SelectItem(selectedItem);
				c.ForceRedraw();
			}
			// args.RetVal = true;
			return true;
		}

		if (e.Key.ToString() == "space" || e.Key.ToString() == "Return") {
			this.OnItemSelected(selectedItem, null);
			this.OnEditItemRequest(selectedItem, null);
			// ActivateItemAnimation(selectedItem);
			return true;
		}

		row = selectedItem.Row;
		col = selectedItem.Col;
		al = (ArrayList) shelfItems[0];
		i = (Item) al[0];
 		if (e.Key.ToString() == "Left") {
			col--;
			if (col == -1) {
				row--;
				if (row == -1)
					row = shelfItems.Count - 1;
				al = (ArrayList) shelfItems[row];
				col = al.Count - 1;
			}
			al = (ArrayList) shelfItems[row];
			i = (Item) al[col];
		} else if (e.Key.ToString() == "Right") {
			col ++;
			if (col >= ((ArrayList) shelfItems[row]).Count) {
				col = 0;
				row++;
				if (row >= shelfItems.Count) {
					row = 0;
				}
			}
			al = (ArrayList) shelfItems[row];
			i = (Item) al[col];
		} else if (e.Key.ToString() == "Up") {
			row --;
			if (row == -1)
				row = shelfItems.Count - 1;
			al = (ArrayList) shelfItems[row];
			if (col >= al.Count)
				col = al.Count - 1;
			i = (Item) al[col];
		} else if (e.Key.ToString() == "Down") {
			row ++;
			if (row >= shelfItems.Count)
				row = 0;
			al = (ArrayList) shelfItems[row];
			if (col >= al.Count)
				col = al.Count - 1;
			i = (Item) al[col];
		}
		selectedItem.IsSelected = false;
		i.IsSelected = true;
		selectedItem = i;
		/* inform the presentation that we've got a new selection */
		this.OnItemSelected(selectedItem, null);
		/* force a redraw of all the elements because we've got something
           new highlighted */
		c.ForceRedraw();
		return true;
	}


	public int CalculateMaxPos() {
		int maxPos = 0;
		try {
			maxPos = (int) shelfLocations[shelfLocations.Count-1];
			maxPos = maxPos + shelf.Height;
		} catch {
		}
		return maxPos;
	}

	public void CalculatePositions(Catalog catalog)
	{
		#if (DEBUGSHELFTHEME)
		System.Console.WriteLine("ShelfTheme.cs (CalculatePositions): method invoked");
		#endif
		ArrayList thisRow = null;
		ItemCollection ic;
		try {
			ic = catalog.ItemCollection;
		} catch {
			return;
		}
		shelfLocations = new ArrayList();
		shelfItems = new ArrayList();

		ELEMENT_SIZE = (int)(DEFAULT_ELEMENT_SIZE * catalog.Zoom);
		// First, go through an precalculate all of the locations for the
		// graphics based on the current zoom
		int lastX = 0;
		int lastY = ELEMENT_SPACING;

		int maxX;
		try {
			maxX = c.Allocation.Width;
		} catch {
			#if (DEBUGSHELFTHEME)
			System.Console.WriteLine("ShelfTheme.cs (CalculatePositions): no allocation yet");
			#endif
			return;
		}

		int row = 0;
		int col = 0;


		// I'm not sure how much extra performance this gets us
		foreach (Item element in ic) {
			int h = element.Pixbuf.Height;
			double hscale = ((double) element.Pixbuf.Height)/((double) ELEMENT_SIZE);
			int newWidth = (int)(((double) element.Pixbuf.Width)/hscale);
			int newHeight = ELEMENT_SIZE;
			if ((lastX + newWidth + ELEMENT_SPACING * 2) > maxX && lastX != 0) {
				lastX = 0;
				lastY = lastY + ELEMENT_SIZE + ELEMENT_SPACING + shelf.Height;
				row = row + 1;
				col = 0;
			}
		
			// if this is the first element on the row, then add the shelf
			if (lastX == 0) {
				shelfLocations.Add(lastY + ELEMENT_SIZE);
				thisRow = new ArrayList();
				shelfItems.Add(thisRow);
			}
			thisRow.Add(element);
			element.Row = row;
			element.Col = col;
			int xPos = lastX + ELEMENT_SPACING;
			int yPos = lastY;
			lastX = lastX + ELEMENT_SPACING + newWidth;
			// create a Rect based on the current positions
			element.Rect = new Gdk.Rectangle(xPos, yPos, newWidth, newHeight);
			col = col + 1;
		}
	}

	/// <summary>
	/// Called to draw the borrwed icon on an item
	/// </summary>
	/// <param name="i">Item to draw on</param>
	/// <param name="c">Cairo.Context object to draw on</param>
	public void DrawBorrowed (Item i, Cairo.Context c) {
		Cairo.Matrix m = new Cairo.Matrix();
		c.Save();
		int xpos = i.Rect.X + i.Rect.Width - borrowedImage.Width;
		int ypos = i.Rect.Y + i.Rect.Height - borrowedImage.Height;
		m.InitTranslate(-1*(xpos),
						-1*(ypos));
		borrowedImagePattern.Matrix = m;
		c.Rectangle(xpos, ypos, borrowedImage.Width, borrowedImage.Height);
		c.Pattern = borrowedImagePattern;
		c.Fill();
		c.Restore();
	}

	/// <summary>Return the image of the shelf background</summary>
	public ImageSurface ShelfBG {
		get {
			return shelfbg;
		}
	}

	public SurfacePattern ShelfBGPattern {
		get {
			return shelfbgPattern;
		}
	}

	/// <summary>Return the image of the shelf</summary>
	public ImageSurface Shelf {
		get {
			return shelf;
		}
	}

	/// <summary>Return the name of the theme</summary>
	public string Name {
		get {
			return name;
		}
	}

	protected void ActivateItemAnimation(Item e) {
		e.ScaleFactor = 1.6;
		if (this.animateActive == false) {
			this.animateActive = true;
			GLib.Timeout.Add(25, new GLib.TimeoutHandler(AnimateTimeout));
		}
	}

	/// <summary>
	/// This handles animations for selected items
	/// </summary>
	private bool AnimateTimeout()
	{
		ItemCollection ic = c.Catalog.ItemCollection;
		bool retVal = false;
		foreach (Item element in ic) {
			if (element.ScaleFactor > 1) {
				element.ScaleFactor -= 0.2;
				retVal = true;
			}
 		}
		// FIXME: this should probably be handled better from a performance aspect
		c.ForceRedraw();
		this.animateActive = retVal;
		return retVal;
	}


}
}
