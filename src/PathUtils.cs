using System.Text.RegularExpressions;

namespace MCatalog {
public class PathUtils {

	/// <summary>Strips invalid characters from filenames</summary>
	public static string StripNonFilename(string arg) {
		return Regex.Replace(arg, @"[^A-Z0-9a-z\-_]", "");
	}

	public static string GetExtension(string arg) {
		string[] splits = arg.Split('.');
		return splits[splits.Length-1];
	}

	public static string RemoveExtension(string arg) {
		string [] splits = arg.Split('.');
		string lastString = "";
		for (int i = 0; i < splits.Length - 1; i ++) {
			lastString += splits[i];
		}
		return lastString;
	}

	public static string RemovePath(string args) {
		string [] splits = args.Split(System.IO.Path.DirectorySeparatorChar);
		return splits[splits.Length - 1];
	}
}
}
