/*
 * Copyright (C) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections;
using System.Reflection;

using Gtk;
using Gdk;
using Gnome;

namespace MCatalog {
public abstract class AddDialog: Gtk.Dialog
{
	protected Catalog catalog;
	protected ArrayList values;
	protected string searchCategory;

	protected Gtk.Button cancelButton;
	protected Gtk.Button okButton;
	protected RatingWidget ratingWidget;
	protected ComboBox comboBoxSearchEngine;

	[Glade.Widget] protected Gtk.Button buttonSearch;
	[Glade.Widget] protected Gtk.Entry entryTitle;
	[Glade.Widget] protected Gtk.HBox hboxSearch;
	[Glade.Widget] protected Gtk.HBox hboxItemRating;
	[Glade.Widget] protected Gtk.HBox addItemHBox;

	// these items are present in all entries
	[Glade.Widget] protected Gtk.Image itemImage;
	[Glade.Widget] protected Gtk.FileChooserButton imageChooserButton;

	protected int Update = -1;

	protected SearchEngine searchEngine;
	protected ArrayList searchEngines = null;
	protected ProgressDialog progressDialog;
	protected ArrayList list;
	protected SearchResults results;
	protected string image;

	protected Thread thread;
	protected ThreadNotify notify;

	/// <summary>
	/// This is the new constructor for the add/edit item dialogs.
	/// It usese the base stuff from addItemDialog and then adds in
	/// stuff based on whatever the other document passed in is.
	/// </summary>
	public AddDialog (Catalog catalog,
	                  string widgetName,
	                  string title,
	                  string searchCategory) {
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): specified widget constructor called");
		#endif
		Glade.XML gxml = new Glade.XML ("mainwindow.glade", "addItemDialogVBox");
		Glade.XML gxmlTable = new Glade.XML ("mainwindow.glade", widgetName);

		gxml.Autoconnect(this);
		gxmlTable.Autoconnect(this);

		this.catalog = catalog;
		this.values = new ArrayList();

		this.searchCategory = searchCategory;

		// FIXME: at some point this should be part of the actual glade file, no?
		cancelButton = (Button)this.AddButton (Gtk.Stock.Cancel, 0);
		okButton     = (Button)this.AddButton (Gtk.Stock.Ok, 1);
		cancelButton.Clicked += OnCancelButtonClicked;
		okButton.Clicked     += OnOkButtonClicked;
		okButton.Sensitive = false;

		VBox vBox = this.VBox;
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): getting widget");
		#endif
		vBox.Add ((VBox)gxml["addItemDialogVBox"]);
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): widget found {0}", (VBox)gxml["addItemDialogVBox"]);
		#endif

		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): adding in item specific widgets");
		#endif
		addItemHBox.Add(gxmlTable.GetWidget(widgetName));
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): widget found {0}", gxmlTable.GetWidget(widgetName));
		#endif
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): set entry title properties {0}", gxmlTable.GetWidget("entryTitle"));
		#endif
		entryTitle.Changed += OnEntryTitleChanged;
		entryTitle.IsFocus = true;
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): set entry title properties");
		#endif
		ratingWidget = new RatingWidget();
		ratingWidget.Visible = true;
		hboxItemRating = (Gtk.HBox)gxmlTable.GetWidget("hboxItemRating");
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): hboxItemRating {0}", hboxItemRating);
		#endif
		hboxItemRating.Add (ratingWidget);

		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): buttonSearch {0}", buttonSearch);
		#endif
		buttonSearch.Clicked += OnSearchClicked;

		comboBoxSearchEngine = CreateComboBoxSearchEngine ();

		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs (newctor): hboxSearch {0}", hboxSearch);
		#endif
		hboxSearch.PackEnd (comboBoxSearchEngine);

		// add the new handler for the image chooser button
		imageChooserButton.SelectionChanged += new EventHandler(OnImageChooserButtonFileChanged);

		this.ShowAll();
	}

	public abstract void EditItem (Item item);
	protected abstract void FillDialogFromSearch (SearchResults results);
    public abstract void OnOkButtonClicked (object o, EventArgs args);

	/// <summary>
	/// Utility function to download images - gives a dummy prefix message.
	/// </summary>
	public string GetImage (string url)
	{
		return GetImage(url, "downloaded");
	}

	/// <summary>
	/// Downloads an image from a remote host.  As an example, one would want
	/// to provide a prefix to go before the filename.
	/// </summary>
	/// <param>url - the url of the image to load</param>
	/// <param>prefix - the prefix to put before the file name</param>
	public string GetImage (string url, string prefix)
	{
		WebClient client = new WebClient ();
		string tempFile = Conf.DownloadedImagesDir + "/" + PathUtils.StripNonFilename(prefix) + PathUtils.RemovePath(PathUtils.RemoveExtension(System.IO.Path.GetTempFileName()));  // name of the downloaded file
		tempFile += "." + PathUtils.GetExtension(url);
		File.Delete (tempFile);
		System.Console.WriteLine("dialog/AddDialog.cs (GetImage): downloading {0} to {1}", url, tempFile);
		client.DownloadFile (url, tempFile);
		return tempFile;
	}

	public void OnEntryTitleChanged (object sender, EventArgs e)
	{
		if (!entryTitle.Text.Trim().Equals ("")) {
			okButton.Sensitive = true;
		}
		else {
			okButton.Sensitive = false;
		}
	}

	public void OnCancelButtonClicked (object o, EventArgs args)
	{
		this.Destroy();
	}

	protected ComboBox CreateComboBoxSearchEngine ()
	{
		bool isApplicable = false;
		searchEngines = new ArrayList ();

		ComboBox combo = ComboBox.NewText ();

		Assembly assembly = Assembly.GetCallingAssembly();
		Type type = typeof (SearchEngine);
		foreach (Type t in assembly.GetTypes()) {
			
			isApplicable = false;


			if (t.IsSubclassOf (type)) {
				object[] attributes = t.GetCustomAttributes(typeof(SearchEngineAttribute), false);
				foreach(object attribute in attributes)
				{
					SearchEngineAttribute sea = (SearchEngineAttribute) attribute;
					if (sea.Category.Equals (searchCategory)) {
						isApplicable = true;
					}
				}

				if (isApplicable) {
					SearchEngine searchEngine = (SearchEngine)Activator.CreateInstance (t);
					combo.AppendText (searchEngine.Name);
					searchEngines.Add (searchEngine);
				}
			}
		}

		combo.Active = 0;
		return combo;
	}	

	protected void OnSearchClicked (object o, EventArgs args)
	{
		searchEngine = (SearchEngine)searchEngines [comboBoxSearchEngine.Active];

		if (entryTitle.Text.Trim().Equals("")) {
			string message = (Mono.Unix.Catalog.GetString ("You must write something to search."));

			MessageDialog dialog = new MessageDialog (null,
					DialogFlags.Modal | DialogFlags.DestroyWithParent,
					MessageType.Warning,
					ButtonsType.Close,
					message);
			dialog.Run ();
			dialog.Destroy ();

		}
		else {
			progressDialog = new ProgressDialog(searchEngine.Name);
			progressDialog.ShowAll();
			progressDialog.Response += OnProgressDialogResponse;

			thread = new Thread(new ThreadStart (doQuery));
			notify = new ThreadNotify (new ReadyEvent (CreateSelectionDialog));

			thread.Start();
		}
	}

	protected void CreateSelectionDialog ()
	{
		progressDialog.CloseDialog ();
	}

	protected void OnProgressDialogResponse (object o, EventArgs args)
	{
		if (list != null && list.Count > 0) {
			SelectDialog listDialog = new SelectDialog (list, searchCategory);
			int selection = listDialog.Run();
			System.Console.WriteLine("dialog/AddDialog.cs (OnProgressDialogResponse): search response {0}", selection);
			if (selection != -1) {
				results = (SearchResults)list[selection];
				FillDialogFromSearch (results);
			}
		}
		else {
			string message = (Mono.Unix.Catalog.GetString ("No matches for your query"));

			MessageDialog dialog = new MessageDialog (null,
					DialogFlags.Modal | DialogFlags.DestroyWithParent,
					MessageType.Warning,
					ButtonsType.Close,
					message);
			dialog.Run ();
			dialog.Destroy ();
		}
	}

	protected void doQuery ()
	{
		list = searchEngine.Query (searchCategory, entryTitle.Text);
		notify.WakeupMain ();
	}

	/// <summary>called when the filename is changed</summary>
	private void OnImageChooserButtonFileChanged(object sender, EventArgs e)
	{
		if (imageChooserButton.Filename == null) {
			#if (DEBUG)
			System.Console.WriteLine("dialogs/AddDialog.cs(OnImageChooserButtonFileChanged): not setting image to null string");
			#endif
			itemImage.Pixbuf = null;
			return;
		}
		#if (DEBUG)
		System.Console.WriteLine("dialogs/AddDialog.cs(OnImageChooserButtonFileChanged): setting image to {0}", imageChooserButton.Filename);
		#endif
		try {
			Pixbuf p = new Pixbuf(imageChooserButton.Filename);
			Gdk.Rectangle rect = itemImage.Allocation;
			double widthRatio;
			double heightRatio;
			int newX;
			int newY;
			widthRatio = (double) rect.Width / (double) p.Width;
			heightRatio = (double) rect.Height / (double) p.Height;

			if (widthRatio < heightRatio)
				heightRatio = widthRatio;
			else
				widthRatio = heightRatio;
			newX = (int)((double)p.Width * widthRatio);
			newY = (int)((double)p.Height * heightRatio);
			itemImage.Pixbuf = p.ScaleSimple(newX,newY,InterpType.Bilinear);
		} catch {
			System.Console.WriteLine("dialogs/AddDialog.cs(OnImageChooserButonFileChanged): error in setting image" + imageChooserButton.Filename);
			return;
		}
	}
}
}
