/*
 * Copyright (C) 2004 Cesar Garcia Tapia <tapia@mcatalog.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

using System;
using System.IO;
using System.Text;
using System.Data;
using System.Reflection;
using Mono.Data.SqliteClient;

using System.Collections;
using System.Collections.Specialized;

using Gdk;
using Mono.Unix;

namespace MCatalog {
// Database tables are defined in Database.sql

public class Database
{
	SqliteConnection connection;
	SqliteCommand command;
	string dbVersion;

	bool debug;

	public Database (string file)
	{
		bool create = false;
		if (!File.Exists (file)) {
			create = true;
		}

		try {
			StreamReader srVersion = new StreamReader (file+".version");
			dbVersion = srVersion.ReadToEnd ();
			if (dbVersion != null) {
				dbVersion = dbVersion.Trim();
			}
			srVersion.Close();
		}
		catch {
			dbVersion = null;
		}

		string connectionString = "URI=file:"+file;
		connection = new SqliteConnection(connectionString);
		connection.Open();
		command = connection.CreateCommand();

		if (create) {
			Conf.EmptyCache();
			Assembly thisAssembly = Assembly.GetEntryAssembly ();
			Stream stream = thisAssembly.GetManifestResourceStream("Database.sql");
			if (stream != null) {
				StreamReader sr = new StreamReader (stream);
				string sql = sr.ReadToEnd();
				command.CommandText = sql;
				command.ExecuteNonQuery();

				StreamWriter swVersion = new StreamWriter (file+".version", false);
				swVersion.Write (Defines.VERSION);
				swVersion.Close();

				dbVersion = Defines.VERSION;
			}
			else {
				System.Console.WriteLine("Error creating the database");
			}
		}

		if (dbVersion == null || !dbVersion.Equals (Defines.VERSION)) {
			UpdateDatabase(file, dbVersion);
		}
	}

	public void Close ()
	{
		command.Dispose();
		connection.Close();
	}

	public bool Debug {
		get {
			return debug;
		}
		set {
			debug = value;
		}
	}

	private static string SqlString (string s)
	{
		return s.Replace ("'", "''");
	}

	public ArrayList Catalogs ()
	{
		#if (DEBUGDATABASE)
		Console.WriteLine("Database.cs (Catalogs): method invoked");
		#endif

		ArrayList array = new ArrayList();

		command.CommandText = "SELECT * FROM catalogs ORDER BY weight";
		IDataReader reader = command.ExecuteReader();
		while(reader.Read()) {
			Catalog catalog = new Catalog (this,
					reader["name"].ToString(),
					reader["table_name"].ToString(),
					reader["short_description"].ToString(),
					reader["long_description"].ToString(),
					reader["image"].ToString(),
					Int32.Parse(reader["weight"].ToString()));
			array.Add (catalog);
		}
		reader.Close();

		#if (DEBUGDATABASE)
		Console.WriteLine("Database.cs (Catalogs): method exited");
		#endif

		return array;
	}

	public View GetCatalogView (string catalog)
	{
		#if (DEBUGDATABASE)
		Console.WriteLine("Database.cs (GetCatalogView): method invoked {0}", catalog);
		#endif
		
		try {
			string sql = "SELECT view FROM catalogs WHERE name='"+catalog+"'";
			command.CommandText = sql;
			IDataReader reader = command.ExecuteReader();
			reader.Read();

			#if (DEBUGDATABASE)
			Console.WriteLine("Database.cs (GetCatalogView): method exited");
			#endif

			return (View)Int32.Parse (reader["view"].ToString());
		}
		catch {
			#if (DEBUGDATABASE)
			Console.WriteLine("Database.cs (GetCatalogView): method exited");
			#endif

			return (View)1;
		}
	}

	public void SetCatalogView (string catalog, View view)
	{
		#if (DEBUGDATABASE)
		Console.WriteLine("Database.cs (SetCatalogView): method invoked {0} {1}", catalog, view);
		#endif

		string sql = "UPDATE catalogs SET view="+(int)view+" WHERE name='"+catalog+"'";
		command.CommandText = sql;
		command.ExecuteNonQuery();
		
		#if (DEBUGDATABASE)
		Console.WriteLine("Database.cs (SetCatalogView): method exited");
		#endif
	}

	public Order GetCatalogOrder (string catalog)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetCatalogOrder): method invoked");
		#endif

		string sql ="SELECT order_by, sort FROM catalogs WHERE name='"+catalog+"'";
		command.CommandText = sql;
		IDataReader reader = command.ExecuteReader();
		reader.Read();
		Order order = new Order ();
		order.Field = reader["order_by"].ToString();
		order.Ascending = reader["sort"].ToString().Equals("ascending")?true:false;

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetCatalogOrder): method exited");
		#endif

		return order;
	}
	
	public void SetCatalogOrder (string catalog, Order order)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (SetCatalogOrder): method invoked {0} {1}", catalog, order);
		#endif

		string sort;
		if (order.Ascending) {
			sort = "ascending";
		}
		else {
			sort = "descending";
		}
		string sql = "UPDATE catalogs SET ";
		
		if (order.Field != null && !order.Field.Trim().Equals("")) {
			sql += "order_by='"+order.Field+"', ";
		}
		
		sql += "sort='"+sort+"' WHERE name='"+catalog+"'";

		command.CommandText = sql;
		command.ExecuteNonQuery();

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (SetCatalogOrder): method exited");
		#endif
	}

	public double GetCatalogZoom (string catalog)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetCatalogZoom): method invoked {0}", catalog);
		#endif

		try {
			string sql = "SELECT zoom FROM catalogs WHERE name='"+catalog+"'";
			command.CommandText = sql;
			IDataReader reader = command.ExecuteReader();
			reader.Read();

			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (GetCatalogZoom): method exited");
			#endif

			return Double.Parse (reader["zoom"].ToString(), System.Globalization.NumberFormatInfo.InvariantInfo);
		}
		catch {
			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (GetCatalogZoom): method exited");
			#endif
			return (double)1;
		}
	}

	public void SetCatalogZoom (string catalog, double zoom)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (SetCatalogZoom): method invoked {0} {1}", catalog, zoom);
		#endif

		string zoomString = zoom.ToString (System.Globalization.NumberFormatInfo.InvariantInfo);
		string sql = "UPDATE catalogs SET zoom="+zoomString+" WHERE name='"+catalog+"'";
		command.CommandText = sql;
		command.ExecuteNonQuery();
		
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (SetCatalogZoom): method exited");
		#endif
	}
	
	public ItemList LoadItemList (Catalog catalog, Presentation presentation)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (LoadItemList): method invoked");
		#endif

		ListDictionary columns = GetColumns (catalog.Table);
		Hashtable columnsToShow = GetColumnsToShow (catalog.Table);
		ItemList list = new ItemList (catalog, columns, columnsToShow, presentation);

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (LoadItemList): method exited");
		#endif		
		return list;
	}

	public ArrayList GetItems (string table, string catalog)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetItems): method invoked {0} {1}", table, catalog);
		#endif

		Order order = this.GetCatalogOrder (catalog);
		ArrayList list = new ArrayList ();
		
		string sql = "SELECT id FROM "+table;
		
		if (order.Field!=null && !order.Field.Trim().Equals("")) {
			sql+=" ORDER BY "+order.Field;
			if (order.Ascending) {
				sql += " ASC";
			}
			else {
				sql += " DESC";
			}
		}
		
		command.CommandText = sql;
		IDataReader reader = command.ExecuteReader();

		while (reader.Read()) {
			list.Add (new Item (this, table, Int32.Parse (reader["id"].ToString()), false));
		}

		reader.Close();

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetItems): method exited");
		#endif

		return list;
	}

	public string GetItemCover (string table, int id)
	{		
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetItemCover): method invoked {0} {1}", table, id);
		#endif
		string sql = "SELECT image FROM "+table+" WHERE id="+id;
		command.CommandText = sql;
		IDataReader reader = command.ExecuteReader();
		reader.Read();
		return (string)reader["image"];
	}

	public ArrayList Search (string searchString, string table)
	{
		return Search (searchString, table, null);
	}

	public ArrayList Search (string searchString, string table, string field)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (Search): method invoked {0} {1} {2}", searchString, table, field);
		#endif

		StringBuilder sql = new StringBuilder ();
		sql = sql.Append ("SELECT id FROM ");
		sql = sql.Append (table);
		sql = sql.Append (" WHERE ");
		
		ListDictionary columnNames = this.GetColumns (table);
		int i = 0;
		foreach (string s in columnNames.Keys) {
			if (i++!=0) {
				sql = sql.Append (" OR ");
			}

			sql = sql.Append (s);
			sql = sql.Append (" LIKE '%");
			sql = sql.Append (SqlString(searchString));
			sql = sql.Append ("%'");
		}

		if (field!=null)
			sql = sql.Append (" ORDER BY "+field);

		command.CommandText = sql.ToString();
		IDataReader reader = command.ExecuteReader();

		ArrayList list = new ArrayList ();
		while (reader.Read()) {
			int id = Int32.Parse(reader["id"].ToString());
			list.Add (new Item (this, table, id, false));
		}

		reader.Close();

		
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (Search): method exited", searchString, table, field);
		#endif

		return list;
	}
	
	public ArrayList GetBorrowers ()
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetBorrowers): method invoked");
		#endif

		string sql = "SELECT * FROM borrowers";
		command.CommandText = sql;

		SqliteDataReader reader = command.ExecuteReader();
		
		Hashtable names = new Hashtable();
		while (reader.Read()) {
			names.Add (Int32.Parse(reader["id"].ToString()), reader["name"].ToString());
		}
		reader.Close();
		
		ArrayList list = new ArrayList ();
		foreach (int id in names.Keys) {
			sql = "SELECT * FROM lends WHERE borrower="+id;
			command.CommandText = sql;
			reader = command.ExecuteReader();
			
			ArrayList items = new ArrayList ();
			while (reader.Read()) {
				items.Add (Int32.Parse(reader["id"].ToString()));
			}
			list.Add (new Borrower (id, (string)names[id], items));
			reader.Close();
		}

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetBorrowers): method exited");
		#endif

		return list;
	}

	public Item GetBorrowerItem (int id)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetBorrowerItem): method invoked {0}", id);
		#endif


		string sql = "SELECT * FROM lends WHERE id="+id;
		command.CommandText = sql;
		SqliteDataReader reader = command.ExecuteReader();
		if (reader.Read()) {
			if (reader["item_id"] != null && !reader["item_id"].ToString().Equals("")) {
				string table = (string)reader["table_name"];
				int itemId = Int32.Parse(reader["item_id"].ToString());
				reader.Close();

				#if (DEBUGDATABASE)
				System.Console.WriteLine("Database.cs (GetBorrowerItem): method exited");
				#endif

				return new Item (this, table, itemId);
			}
			else {
				#if (DEBUGDATABASE)
				System.Console.WriteLine("Database.cs (GetBorrowerItem): method exited");
				#endif

				return null;
			}
		}
		else {
			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (GetBorrowerItem): method exited");
			#endif

			return null;
		}
	}

	public ArrayList LoadBorrowerItems (Borrower borrower, string tableName)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (LoadBorrowerItems): method invoked {0} {1}", borrower.Name, tableName);
		#endif

		ArrayList list = new ArrayList ();
		string sql = "SELECT * from lends WHERE borrower="+borrower.Id;
		command.CommandText = sql;
		SqliteDataReader reader = command.ExecuteReader ();
		while (reader.Read ()) {
			if (reader["item_id"] != null && !reader["item_id"].ToString().Equals("")) {
				string table = (string)reader["table_name"];
				int itemId = Int32.Parse(reader["item_id"].ToString());
				list.Add (new Item (this, table, itemId));
			}
		}
		reader.Close();
	
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (LoadBorrowerItems): method exited");
		#endif

		return list;
	}

	public int CountBorrowerItems(int id)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (CountBorrowerItems): method invoked {0}", id);
		#endif

		string sql = "SELECT count(*) FROM lends WHERE borrower="+id;
		command.CommandText = sql;
		SqliteDataReader reader = command.ExecuteReader();
		if (reader.Read()) {

			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (CountBorrowerItems): method exited");
			#endif

			return Int32.Parse (reader[0].ToString());
		}
		else {
			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (CountBorrowerItems): method exited");
			#endif

			return 0;
		}
	}


	public int LendItem (Item item, Borrower borrower)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (LendItem): method invoked {0} {1}", item.Id, borrower.Name);
		#endif

		StringBuilder sql = new StringBuilder (200);
		sql = sql.Append ("INSERT INTO lends VALUES (NULL, ");
		sql = sql.Append (borrower.Id);
		sql = sql.Append (", '");
		sql = sql.Append (item.Table);
		sql = sql.Append ("', ");
		sql = sql.Append (item.Id);
		sql = sql.Append (")");
		
		command.CommandText = sql.ToString();
		command.ExecuteNonQuery();

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (LendItem): method exited");
		#endif

		return ((SqliteCommand)command).LastInsertRowID();
	}
	
	public void ReturnItem (Item item)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (ReturnItem): method invoked {0}", item.Id);
		#endif

		// FIXME: this should not be delete, we want a lending history
		string sql = "DELETE FROM lends WHERE item_id="+item.Id;
		command.CommandText = sql;
		command.ExecuteNonQuery();
	}
	
	public bool IsBorrowed (Item item)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (IsBorrowed): method invoked {0}", item.Id);
		#endif

		StringBuilder sql = new StringBuilder (100);
		sql = sql.Append ("SELECT * FROM lends WHERE item_id=");
		sql = sql.Append (item.Id);
		sql = sql.Append (" AND table_name='");
		sql = sql.Append (item.Table);
		sql = sql.Append ("'");
		command.CommandText = sql.ToString();

		SqliteDataReader reader = command.ExecuteReader();
		if (reader.Read()) {
			reader.Close();
			return true;
		}
		else {
			reader.Close();
			return false;
		}
	}

	public int AddBorrower (string name)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (AddBorrower): method invoked {0}", name);
		#endif

		// FIXME: we should not necessarily rely on the database schema to be correct here
		string sql = "INSERT INTO borrowers VALUES (NULL, '"+name+"')";
		command.CommandText = sql;
		command.ExecuteNonQuery();

		return ((SqliteCommand)command).LastInsertRowID();
	}
	
	public ListDictionary GetColumns (string table)
	{
		return InternationalizeDataBaseFields (table);
	}

	public Hashtable GetColumnsToShow (string table)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetColumnsToShow): method invoked {0}", table);
		#endif

		string sql = "SELECT * FROM "+table+"_columns";
		command.CommandText = sql;

		IDataReader reader = command.ExecuteReader();
		if (reader.Read()) {
			Hashtable columnsToShow = new Hashtable ();
			int i = 0;
			while (i < reader.FieldCount) {
				string campo = reader.GetName(i).ToString();
				if (!campo.Equals("aux")) {
					columnsToShow.Add (campo, reader[campo].ToString().Equals("Y")?true:false);
				}
				i++;
			}

			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (GetColumnsToShow): method exited");
			#endif

			return columnsToShow;
		}
		else {
			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (GetColumnsToShow): method exited");
			#endif

			return null;
		}
	}

	public void SetColumnsToShow (string table, Hashtable hashtable)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (SetColumnsToShow): method invoked {0}", table);
		#endif

		StringBuilder sql = new StringBuilder (1000);
		sql = sql.Append ("BEGIN; ");
		foreach (string s in hashtable.Keys) {
			sql = sql.Append ("UPDATE ");
			sql = sql.Append (table);
			sql = sql.Append ("_columns SET ");
			sql = sql.Append (s);
			sql = sql.Append ("='");
			if ((bool)hashtable[s]) {
				sql = sql.Append ("Y");
			}
			else {
				sql = sql.Append ("N");
			}
			sql = sql.Append ("' WHERE aux='1'; ");
		}
		sql = sql = sql.Append ("COMMIT;");
		command.CommandText = sql.ToString();
		command.ExecuteNonQuery();
		
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (SetColumnsToShow): method exited");
		#endif
	}

	public int AddItem (string table, Hashtable itemInfo)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (AddItem): method invoked {0} {1}", table, itemInfo);
		#endif

		StringBuilder sql = new StringBuilder (1000);
		sql = sql.Append ("INSERT INTO ");
		sql = sql.Append (table);
		sql = sql.Append (" (id, ");

		int i = 0;
		foreach (string key in itemInfo.Keys) {
			sql = sql.Append (key);
			
			if (i!=itemInfo.Count-1) {
				sql = sql.Append (", ");
			}
			else {
				sql = sql.Append (")");
			}
			i++;
		}
		
		sql = sql.Append (" VALUES (NULL, ");

		i = 0;
		foreach (string key in itemInfo.Keys) {
			StringBuilder aux = new StringBuilder(100);
			if (itemInfo[key]!=null) {
				aux = aux.Append ("'");
				aux = aux.Append (SqlString(itemInfo[key].ToString()));
				aux = aux.Append ("'");
			}
			else {
				aux = aux.Append ("NULL");
			}
			
			sql = sql.Append (aux);
			
			if (i!=itemInfo.Count-1) {
				sql = sql.Append (", ");
			}
			else {
				sql = sql.Append (")");
			}
			i++;
		}

		command.CommandText = sql.ToString();
		command.ExecuteNonQuery();

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (AddItem): method exited");
		#endif

		return ((SqliteCommand)command).LastInsertRowID();
	}

	public void UpdateItem (string table, int id, Hashtable itemInfo)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (UpdateItem): method invoked {0} {1} {2}", table, id, itemInfo);
		#endif

		ListDictionary columnNames = this.GetColumns (table);
		StringBuilder sql = new StringBuilder (1000);
		sql = sql.Append ("UPDATE ");
		sql = sql.Append (table);

		sql = sql.Append (" SET ");

		int i = 0;
		foreach (string key in columnNames.Keys) {
			sql = sql.Append (key);
			sql = sql.Append ("=");
			if (itemInfo[key] != null) {
				sql = sql.Append ("'");
				sql = sql.Append (SqlString(itemInfo[key].ToString()));
				sql = sql.Append ("'");
			}
			else {
				sql = sql.Append ("NULL");
			}

			if (i < columnNames.Count - 1) {
				sql = sql.Append (", ");
			}
			i++;
		}

		sql = sql.Append (" WHERE id=");
		sql = sql.Append (id);

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (UpdateItem): SQL: {0}", sql.ToString());
		#endif

		command.CommandText = sql.ToString();
		command.ExecuteNonQuery();

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (UpdateItem): method exited");
		#endif
	}

	public void RemoveItem (string table, int itemId)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (RemoveItem): method invoked {0} {1}", table, itemId);
		#endif

		StringBuilder sql = new StringBuilder (100);
		sql = sql.Append ("DELETE FROM ");
		sql = sql.Append (table);
		sql = sql.Append (" WHERE id=");
		sql = sql.Append (itemId);
		command.CommandText = sql.ToString();
		command.ExecuteNonQuery();
		Item.RemoveCache (table, itemId);
		

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (RemoveItem): method exited");
		#endif
	}

	public Hashtable GetItemInfo (string table, int id)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetItemInfo): method invoked {0} {1}", table, id);
		#endif

		StringBuilder sql = new StringBuilder (100);
		sql = sql.Append ("SELECT * from ");
		sql = sql.Append (table);
		sql = sql.Append (" WHERE id=");
		sql = sql.Append (id);
		command.CommandText = sql.ToString();
		IDataReader reader = command.ExecuteReader();

		if (reader.Read()) {
			Hashtable info = new Hashtable ();
			int i = 0;
			while (i < reader.FieldCount) {
				string campo = reader.GetName(i).ToString();
				if (reader[i] != null) {
					info.Add (campo, reader[i].ToString());
				}
				else {
					info.Add (campo, null);
				}
				i++;
			}
			reader.Close();


			#if (DEBUGDATABASE)
			System.Console.WriteLine("Database.cs (GetItemInfo): method exited");
			#endif

			return info;
		}

		reader.Close();

		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (GetItemInfo): method exited");
		#endif

		return null;
	}

	private void UpdateDatabase (string file, string dbVersion)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (UpdateDatabase): method invoked {0} {1}", file, dbVersion);
		#endif


		if (!dbVersion.Equals ("0.0.7")) {
			if (!dbVersion.Equals ("0.0.6")) {
				PatchDatabase ("0.0.5.sql");
			}
			PatchDatabase ("0.0.7.sql");
		}

		StreamWriter swVersion = new StreamWriter (file+".version", false);
		swVersion.Write (Defines.VERSION);
		swVersion.Close();
	}

	private void PatchDatabase (string resourceFile)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (PatchDatabase): method invoked {0}", resourceFile);
		#endif
		Assembly thisAssembly = Assembly.GetEntryAssembly ();
		Stream stream = thisAssembly.GetManifestResourceStream (resourceFile);
		if (stream != null) {
			StreamReader sr = new StreamReader (stream);
			string sql = sr.ReadToEnd ();
			command.CommandText = sql;
			command.ExecuteNonQuery ();
		}
	}

	private ListDictionary InternationalizeDataBaseFields (string table_name)
	{
		#if (DEBUGDATABASE)
		System.Console.WriteLine("Database.cs (InternationalizeDataBaseFields): method invoked {0}", table_name);
		#endif
		ListDictionary table = new ListDictionary ();
		switch (table_name) {
			case "items_films":
				table.Add ("id", Mono.Unix.Catalog.GetString ("Id"));
				table.Add ("image", Mono.Unix.Catalog.GetString ("Image"));
				table.Add ("rating", Mono.Unix.Catalog.GetString ("Rating"));
				table.Add ("title", Mono.Unix.Catalog.GetString ("Title"));
				table.Add ("original_title", Mono.Unix.Catalog.GetString ("Original Title"));
				table.Add ("director", Mono.Unix.Catalog.GetString ("Director"));
				table.Add ("starring", Mono.Unix.Catalog.GetString ("Starring"));
				table.Add ("date", Mono.Unix.Catalog.GetString ("Date"));
				table.Add ("genre", Mono.Unix.Catalog.GetString ("Genre"));
				table.Add ("runtime", Mono.Unix.Catalog.GetString ("Runtime"));
				table.Add ("country", Mono.Unix.Catalog.GetString ("Country"));
				table.Add ("language", Mono.Unix.Catalog.GetString ("Language"));
				table.Add ("distributor", Mono.Unix.Catalog.GetString ("Distributor"));
				table.Add ("medium", Mono.Unix.Catalog.GetString ("Medium"));
				table.Add ("comments", Mono.Unix.Catalog.GetString ("Comments"));

				break;

			case "items_books":
				table.Add ("id", Mono.Unix.Catalog.GetString ("Id"));
				table.Add ("image", Mono.Unix.Catalog.GetString ("Image"));
				table.Add ("rating", Mono.Unix.Catalog.GetString ("Rating"));
				table.Add ("title", Mono.Unix.Catalog.GetString ("Title"));
				table.Add ("original_title", Mono.Unix.Catalog.GetString ("Original Title"));
				table.Add ("author", Mono.Unix.Catalog.GetString ("Author"));
				table.Add ("date", Mono.Unix.Catalog.GetString ("Date"));
				table.Add ("genre", Mono.Unix.Catalog.GetString ("Genre"));
				table.Add ("pages", Mono.Unix.Catalog.GetString ("Pages"));
				table.Add ("publisher", Mono.Unix.Catalog.GetString ("Publisher"));
				table.Add ("isbn", Mono.Unix.Catalog.GetString ("ISBN"));
				table.Add ("country", Mono.Unix.Catalog.GetString ("Country"));
				table.Add ("language", Mono.Unix.Catalog.GetString ("Language"));
				table.Add ("comments", Mono.Unix.Catalog.GetString ("Comments"));

				break;

			case "items_albums":
				table.Add ("id", Mono.Unix.Catalog.GetString ("Id"));
				table.Add ("image", Mono.Unix.Catalog.GetString ("Image"));
				table.Add ("rating", Mono.Unix.Catalog.GetString ("Rating"));
				table.Add ("title", Mono.Unix.Catalog.GetString ("Title"));
				table.Add ("author", Mono.Unix.Catalog.GetString ("Author"));
				table.Add ("label", Mono.Unix.Catalog.GetString ("Label"));
				table.Add ("date", Mono.Unix.Catalog.GetString ("Date"));
				table.Add ("style", Mono.Unix.Catalog.GetString ("Style"));
				table.Add ("asin", Mono.Unix.Catalog.GetString ("ASIN"));
				table.Add ("tracks", Mono.Unix.Catalog.GetString ("Tracks"));
				table.Add ("medium", Mono.Unix.Catalog.GetString ("Medium"));
				table.Add ("runtime", Mono.Unix.Catalog.GetString ("Runtime"));
				table.Add ("comments", Mono.Unix.Catalog.GetString ("Comments"));

				break;
		}

		return table;
	}
}
}
