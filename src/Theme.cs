/*
 * Copyright (C) 2006 Patrick Wagstrom <pwagstro@andrew.cmu.edu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Theme.cs contains theme definitions forMCatalog.  These are intended
 * to be the basic commands that any Theme must implement
 */

using Cairo;

namespace MCatalog {
public class Theme
{
	protected string name;

	public Theme(string theme) {
		this.name = theme;
	}

	/// Function that is called to draw the shelves
	public virtual void Draw(Cairo.Context g, CairoShelf c) {
	}

	/// Function called for button presses
	public virtual void ButtonPress(Gdk.EventButton ev) {
	}

}
}
