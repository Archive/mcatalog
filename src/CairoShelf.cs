/*
 * Copyright (C) 2006 Patrick Wagstrom <pwagstro@andrew.cmu.edu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Collections;
using Gdk;
using Gtk;
using GLib;
using Pango;
using Cairo;

namespace MCatalog {
public class CairoShelf : DrawingArea {
	protected Catalog catalog;

	// event handlers
	public event EventHandler OnItemSelected;       // called when an item is selected
 	public event EventHandler OnEditItemRequest;    // called when an edit of an item is requested

	private Presentation presentation;
 	private ArrayList shelfLocations;
 	private ArrayList shelfItems;
	private ShelfTheme theme;

	public CairoShelf () : base()
	{
		this.CanFocus = true;
// 		this.ExposeEvent += new ExposeEventHandler(OnExposed);
// 		this.ButtonPressEvent += new ButtonPressEventHandler(OnButtonPress);
// 		this.MotionNotifyEvent += new MotionNotifyEventHandler(OnMotionNotify);
// 		this.KeyPressEvent += new KeyPressEventHandler(OnKeyPressEvent);
// 		this.Events = EventMask.ExposureMask | EventMask.ButtonPressMask | 
// 				EventMask.PointerMotionMask | EventMask.KeyPressMask | EventMask.KeyReleaseMask;
		this.Events = EventMask.ButtonPressMask | EventMask.ExposureMask | EventMask.KeyPressMask;
		// Go through the property element to set this, that way we ensure the events get registered
		this.Theme = new ShelfTheme(Conf.Get ("ui/theme", "default"));
	}


	protected override bool OnButtonPressEvent(Gdk.EventButton e)
	{
		#if (DEBUGCAIROSHELF)
		System.Console.WriteLine("CairoShelf.cs (OnButtonPressEvent): invoked");
		#endif
		theme.ButtonPress(e);
		return true;
	}

	// FIXME: is this the correct way to do the draw event?  It seems like it's firing
	// off the draw events FAR too often
	protected override bool OnExposeEvent(Gdk.EventExpose args)
	{
		Gdk.Window win = args.Window;
		Cairo.Context g = Gdk.CairoHelper.Create(win);
		int x, y, w, h, d;
		win.GetGeometry(out x, out y, out w, out h, out d);
		

		// under the new model of interaction, the drawing and what not
		// is all done by the theme itself.  This allows for future radical
		// development in themes, maybe even some not requiring the standard
		// shelf view
		theme.Draw(g, this);
		return true;
	}

// 	protected void ActivateItemAnimation(Item e) {
// 		e.ScaleFactor = 1.6;
// 		if (this.animateActive == false) {
// 			this.animateActive = true;
// 			GLib.Timeout.Add(25, new GLib.TimeoutHandler(AnimateTimeout));
// 		}
// 	}

// 	/// <summary>
// 	/// Handler for eyecandy motion effects
// 	/// </summary>
// 	public void OnMotionNotify(object o, MotionNotifyEventArgs args)
// 	{
// 		Gdk.EventMotion ev = args.Event;
// 		Gdk.Window window = ev.Window;
// 		Gdk.Point pt = new Gdk.Point((int)ev.X, (int)ev.Y);
// 		Gdk.Rectangle rect;
// 		ItemCollection ic = catalog.ItemCollection;
// 		foreach (Item element in ic) {
// 			rect = element.Rect;
// 			if (rect.Contains(pt)) {
// 			}
// 		}
// 	}

// 	/// <summary>
// 	/// Handles keyboard events for the widget.  This basically ignores
// 	/// everything except the arrow keys, space, and return.
// 	/// </summary>
	protected override bool OnKeyPressEvent(Gdk.EventKey e)
	{
		#if (DEBUGCAIROSHELF)
		System.Console.WriteLine("CairoShelf.cs (OnKeyPressEvent): invoked");
		#endif
		theme.KeyPress(e);
		return true;
	}

// 		int row = 0;
// 		int col = 0;
// 		ArrayList al;
// 		Item i;
// 		if (e.Key.ToString() != "Left"  &&
// 			e.Key.ToString() != "Right"  && e.Key.ToString() != "Up"  &&
// 			e.Key.ToString() != "Down" && e.Key.ToString() != "space" &&
// 			e.Key.ToString() != "Return") {
// 				return;
// 		}

// 		// catch the case when nothing is displayed
// 		if (shelfItems.Count == 0) {
// 			return;
// 		}

// 		// selct the first item when nothing is selected
// 		if (selectedItem == null) {
// 			if (shelfItems != null && shelfItems.Count > 0) {
// 				al = (ArrayList) shelfItems[0];
// 				i = (Item) al[0];
// 				i.IsSelected = true;
// 				selectedItem = i;
// 				this.ForceRedraw();
// 			}
// 			args.RetVal = true;
// 			return;
// 		}

// 		if (e.Key.ToString() == "space" || e.Key.ToString() == "Return") {
// 			presentation.Load(catalog.Table, selectedItem);
// 			args.RetVal = true;
// 			ActivateItemAnimation(selectedItem);
// 			return;
// 		}

// 		row = selectedItem.Row;
// 		col = selectedItem.Col;
// 		al = (ArrayList) shelfItems[0];
// 		i = (Item) al[0];
// 		if (e.Key.ToString() == "Left") {
// 			col--;
// 			if (col == -1) {
// 				row--;
// 				if (row == -1)
// 					row = shelfItems.Count - 1;
// 				al = (ArrayList) shelfItems[row];
// 				col = al.Count - 1;
// 			}
// 			al = (ArrayList) shelfItems[row];
// 			i = (Item) al[col];
// 		} else if (e.Key.ToString() == "Right") {
// 			col ++;
// 			if (col >= ((ArrayList) shelfItems[row]).Count) {
// 				col = 0;
// 				row++;
// 				if (row >= shelfItems.Count) {
// 					row = 0;
// 				}
// 			}
// 			al = (ArrayList) shelfItems[row];
// 			i = (Item) al[col];
// 		} else if (e.Key.ToString() == "Up") {
// 			row --;
// 			if (row == -1)
// 				row = shelfItems.Count - 1;
// 			al = (ArrayList) shelfItems[row];
// 			if (col >= al.Count)
// 				col = al.Count - 1;
// 			i = (Item) al[col];
// 		} else if (e.Key.ToString() == "Down") {
// 			row ++;
// 			if (row >= shelfItems.Count)
// 				row = 0;
// 			al = (ArrayList) shelfItems[row];
// 			if (col >= al.Count)
// 				col = al.Count - 1;
// 			i = (Item) al[col];
// 		}
// 		args.RetVal = true;
// 		selectedItem.IsSelected = false;
// 		i.IsSelected = true;
// 		selectedItem = i;
// 		this.ForceRedraw();
// 	}


	/// <summary>
	/// Forces a reallocation of the display
	///
	/// In general, if for some reason the allocation of the display
	/// (number of items, size of items, etc) then ForceReallocate
	/// should be called.  Because in almost all situations a Redraw
	/// is needed after a reallocate, that is also called
	/// </summary>
	public void ForceReallocate()
	{
		#if (DEBUGCAIROSHELF)
		System.Console.WriteLine("CairoShelf.cs (ForceReallocate): invoked");
		#endif
		theme.CalculatePositions(catalog);
		int maxPos = theme.CalculateMaxPos();
		try {
			if (maxPos == 0) { maxPos = this.Parent.Allocation.Height - 5; }
		} catch {
			#if (DEBUGCAIROSHELF)
			System.Console.WriteLine("CairoShelf.cs (ForceReallocate): unable to realize, ouch!");
			#endif
			return;
		}
		this.SetSizeRequest(this.Parent.Allocation.Width, maxPos);
		ForceRedraw();
	}

 	/// <summary>Force a redraw of the screen by invalidation</summary>
	public void ForceRedraw()
	{
		this.GdkWindow.InvalidateRect(this.Allocation, true);
	}

// 	/// <summary>
// 	/// This handles animations for selected items
// 	/// </summary>
// 	private bool AnimateTimeout()
// 	{
// 		ItemCollection ic = catalog.ItemCollection;
// 		bool retVal = false;
// 		foreach (Item element in ic) {
// 			if (element.ScaleFactor > 1) {
// 				element.ScaleFactor -= 0.2;
// 				retVal = true;
// 			}
// 		}
// 		ForceRedraw();
// 		this.animateActive = retVal;
// 		return retVal;
// 	}

// 	/// <summary>Get or set the zoom of the view</summary>
	public double Zoom {
		get {
 			return catalog.Zoom;
		}
		set {
			catalog.Zoom = value;
			ForceReallocate();
		}
	}

	/// <summary>A pointer to the Presentation to show items</summary>
 	public Presentation Presentation {
 		get {
 			return presentation;
 		}
 		set {
			System.Console.WriteLine("****************** presentation set...");
 			presentation = value;
 		}
 	}

 	/// <summary>A pointer to the Theme for the shelf</summary>
	public ShelfTheme Theme {
		get {
			return theme;
		}
		set {
 			theme = value;
			theme.OnItemSelected += SelectItem;
			theme.OnEditItemRequest += EditItem;
			// because the themes have different spacing, we need a reallocate here
			ForceReallocate();
		}
	}

	/// <summary>Catalog of the items for this listing</summary>
	public Catalog Catalog {
		get {
			return catalog;
		}
		set {
			#if (DEBUGCAIROSHELF)
			System.Console.WriteLine("CarioShelf.cs (Catalog): catalog set");
			#endif
			// FIXME: need to reset last selected item
			// this.lastSelectedItem = -1;
			catalog = value;
			// FIXME: are there other items that shoud be called, perhaps on the theme which actually draws the display?
			catalog.OnItemCollectionChanged +=  this.OnCatalogChanged;
			this.Zoom = catalog.Zoom;
			ForceReallocate();
			// this.GrabFocus();
		}
	}

	public void OnCatalogChanged() {
		#if(DEBUGCAIROSHELF)
		System.Console.WriteLine("CairoShelf.cs (OnCatalogChanged): invoked");
		#endif
	}

	public void SelectItem(object o, EventArgs args) {
		#if(DEBUGCAIROSHELF)
		System.Console.WriteLine("CairoShelf.cs (SelectItem): method invoked");
		#endif
		if (presentation == null)
			System.Console.WriteLine("For some reason the presentation is null");
		else
			presentation.Load(catalog.Table, (Item) o);
		// propagate the item selected stuff through
		this.OnItemSelected(o, args);
	}

	public void EditItem(object o, EventArgs args) {
		#if(DEBUGCAIROSHELF)
		System.Console.WriteLine("CairoShelf.cs (EditItem): method invoked");
		#endif
		OnEditItemRequest(o, args);
	}

	public void ClearSelection() {
		#if(DEBUGCAIROSHELF)
		System.Console.WriteLine("CairoShelf.cs (ClearSelection): method invoked");
		#endif
		if (presentation != null)
			presentation.Clear();
		else
			System.Console.WriteLine("For some reason presentation is null");
	}

// 		if (this.lastSelectedItem != -1) {
// 			foreach (Item element in catalog.ItemCollection) {
// 				if (element.Id == this.lastSelectedItem) {
// 					#if(DEBUGCAIROSHELF)
// 					System.Console.WriteLine("CairoShelf.cs (OnCatalogChanged): redrawing presentation window");
// 					#endif
// 					selectedItem = element;
// 					element.IsSelected = true;
// 					presentation.Load(catalog.Table, element);
// 				}
// 			}
// 		}
// 		ForceReallocate();
// 	}


}
}
